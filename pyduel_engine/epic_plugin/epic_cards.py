from pyduel_engine.content.engine_states import CardType as Ct

# Combat Cards
COMBAT_5_1 = {'atk': 5, 'def': 1}
COMBAT_4_2 = {'atk': 4, 'def': 2}
COMBAT_4_1 = {'atk': 4, 'def': 1}
COMBAT_3_3 = {'atk': 3, 'def': 3}
COMBAT_3_2 = {'atk': 3, 'def': 2}
COMBAT_3_1 = {'atk': 3, 'def': 1}
COMBAT_2_4 = {'atk': 2, 'def': 4}
COMBAT_2_3 = {'atk': 2, 'def': 3}
COMBAT_2_2 = {'atk': 2, 'def': 2}
COMBAT_2_1 = {'atk': 2, 'def': 1}
COMBAT_1_5 = {'atk': 1, 'def': 5}
COMBAT_1_4 = {'atk': 1, 'def': 4}
COMBAT_1_2 = {'atk': 1, 'def': 2}

# Special Cards
CALM = {'name': 'Calm', 'card_type': Ct.special,
        'desc': 'Move Anakin up to 8 spaces. If, after playing this card you '
                'have no cards in your hand, draw up to 5 cards.'}
CHILDREN_OF_THE_FORCE = {'name': 'Children Of The Force',
                         'card_type': Ct.special,
                         'desc': 'Move Luke up to 6 spaces. Then move Leia up '
                                 'to 6 spaces. Draw 2 cards'}
CHOKE = {'name': 'Choke', 'card_type': Ct.special,
         'desc': 'Choose any minor character. That character receives 6 '
                 'damage.'}
FLAME_THROWER = {'name': 'Flame Thrower', 'card_type': Ct.special,
                 'desc': 'Flame Thrower does 2 damage to all characters '
                         'adjacent to Jango Fett. You may then move these '
                         'characters up to 3 spaces each.'}
FIRE_UP_THE_JET_PACK = {'name': 'Fire Up The Jet Pack',
                        'card_type': Ct.special,
                        'desc': 'You may move Jango Fett to any empty space. '
                                'Playing this card does not count as an '
                                'action.'}
FORCE_BALANCE = {'name': 'Force Balance', 'card_type': Ct.special,
                 'desc': 'All players discard their hands. Each player draws 3'
                         ' new cards.'}
FORCE_DRAIN = {'name': 'Force Drain', 'card_type': Ct.special,
               'desc': 'Pick 2 cards at random from an opponent\'s hand. That '
                       'player must discard those cards.'}
FORCE_LIFT = {'name': 'Force Lift', 'card_type': Ct.special,
              'desc': 'Turn any character adjacent to Yoda on its side. This '
                      'character cannot move, attack, or defend. At any time, '
                      'any player may discard 3 cards to stand this character '
                      'up.'}
FORCE_LIGHTNING = {'name': 'Force Lightning', 'card_type': Ct.special,
                   'desc': 'Choose any character. That character receives 3 '
                           'damage. The player controlling this character must'
                           ' discard a card at random.'}
FORCE_PUSH_DOOKU = {'name': 'Force Push', 'card_type': Ct.special,
                    'desc': 'Move any character adjacent to to any empty '
                            'space. That character receives 1 damage.'}
FORCE_PUSH_YODA = {'name': 'Force Push', 'card_type': Ct.special,
                   'desc': 'Move any character adjacent to any empty space.'
                           ' That character receives 3 damage.'}
FORCE_QUICKNESS = {'name': 'Force Quickness', 'card_type': Ct.special,
                   'desc': 'Move Obi-Wan up to 8 spaces, then draw a card'}
FUTURE_FORESEEN = {'name': 'Future Foreseen', 'card_type': Ct.special,
                   'desc': 'Look at the top 4 cards of your draw pile. Put one'
                           ' in your hand and put the other 3 cards back on '
                           'top of your draw pile in any order.'}
GAIN_POWER = {'name': 'Gain Power', 'card_type': Ct.special,
              'desc': 'Draw 3 cards'}
GIVE_ORDERS = {'name': 'Give Orders', 'card_type': Ct.special,
               'desc': 'Move Dooku up to 4 spaces. Then move Super Battledroid'
                       ' 1 up to 4 spaces and move Super Battledroid 2 up to 4'
                       ' spaces.'}
I_WILL_NOT_FIGHT_YOU = {'name': 'I Will Not Fight You',
                        'card_type': Ct.special,
                        'desc': 'Choose an opponent. You and the chosen'
                                ' opponent reveal your hands. Both of you '
                                'discard all cards with an attack value '
                                'greater than 1.'}
INSIGHT = {'name': 'Insight', 'card_type': Ct.special,
           'desc': 'Look at any opponent\'s hand. Then choose one card. Your '
                   'opponent must discard the chosen card.'}
ITS_NOT_WISE = {'name': 'Its Not Wise', 'card_type': Ct.special,
                'desc': 'Move any character adjacent to Chewbacca up t 3 '
                        'spaces. That character receives 3 damage.'}
JEDI_MIND_TRICK = {'name': 'Jedi Mind Trick', 'card_type': Ct.special,
                   'desc': 'Take any card from your discard pile and put that '
                           'card in your hand.'}
LET_GO_OF_YOUR_HATRED = {'name': 'Let Go Of Your Hatred',
                         'card_type': Ct.special,
                         'desc': 'Choose an opponent. That opponent chooses '
                                 'and discards 2 cards.'}
LUKES_IN_TROUBLE = {'name': 'Luke\'s In Trouble', 'card_type': Ct.special,
                    'desc': 'If Leia is adjacent to Luke, Luke recovers 3 '
                            'damage. If Luke has been destroyed, Leia recovers'
                            ' 3 damage.'}
MEDITATION = {'name': 'Meditation', 'card_type': Ct.special,
              'desc': 'The Emperor recovers up to 4 damage. Choose an '
                      'opponent. That opponent cannot draw cards during'
                      ' his/her next turn.'}
NEVER_TELL_ME_THE_ODDS = {'name': 'Never Tell Me The Odds',
                          'card_type': Ct.special,
                          'desc': 'Han does 2 damage to all opponents '
                                  'characters Han can attack. Then you may '
                                  'shuffle your discard pile into your draw '
                                  'pile.'}
PROTECTION = {'name': 'Protection', 'card_type': Ct.special,
              'desc': 'If Anakin is alive, Padme recovers 4 damage. If '
                      'Anakin has been destroyed, Padme recovers 2 '
                      'damage.'}
ROYAL_COMMAND = {'name': 'Royal Command', 'card_type': Ct.special,
                 'desc': 'Exchange spaces between the Emperor and any Crimson '
                         'Guard.'}
THERMAL_DETONATOR = {'name': 'Thermal Detonator', 'card_type': Ct.special,
                     'desc': 'Thermal Detonator does 4 damage to any one '
                             'character Boba Fett can attack. All character '
                             'adjacent to that character also receive 4 '
                             'damage.'}
THROW_DEBRIS = {'name': 'Throw Debris', 'card_type': Ct.special,
                'desc': 'Choose any character. That character receives 4 '
                        'damage.'}
WHIRLWIND = {'name': 'Whirlwind', 'card_type': Ct.special,
             'desc': 'Mace does 4 damage to all opponents characters he can '
                     'attack.'}
WISDOM = {'name': 'Wisdom', 'card_type': Ct.special,
          'desc': 'You may move Mace up to 5 Spaces, then draw a card.'}
WOOKIE_HEALING = {'name': 'Wookie Healing', 'card_type': Ct.special,
                  'desc': 'Chewbacca recovers up to 3 damage. The you may move'
                          ' Chewbacca up to 5 spaces.'}
WOOKIE_INSTINCTS = {'name': 'Wookie Instincts', 'card_type': Ct.special,
                    'desc': 'Search your draw pile for the Bowcaster Attack '
                            'card. If it is in your draw pile, put it in your '
                            'hand. Then shuffle your draw pile.'}
WRATH_ANAKIN = {'name': 'Wrath', 'card_type': Ct.special,
                'desc': 'You may move Anakin adjacent to any minor Character. '
                        'That character receives 7 damage.'}
WRATH_VADER = {'name': 'Wrath', 'card_type': Ct.special,
               'desc': 'Choose an opponent. All of that opponent\'s characters'
                       ' receive 2 damage.'}
WRIST_CABLE = {'name': 'Wrist Cable', 'card_type': Ct.special,
               'desc': 'Wrist Cable does 2 damage to any one character Fett '
                       'can attack. The player controlling the attacked '
                       'character gets 1 less action on his/her next turn'}
YOU_WILL_DIE = {'name': 'You Will Die', 'card_type': Ct.special,
                'desc': 'Choose an opponent. That opponent must discard '
                        'his/her entire hand.'}
YOUR_SKILLS_ARE_NOT_COMPLETE = {
    'name': 'Your Skills Are Not Complete', 'card_type': Ct.special,
    'desc': 'Choose any opponent. That opponent must reveal his/her hand and '
            'discard all special cards.'}
SUDDEN_ARRIVAL = {'name': 'Sudden Arrival', 'card_type': Ct.special,
                  'desc': 'Move Greedo adjacent to any character. Playing this'
                          ' card does not count as an action.'}

# Special Combat Cards
BATTLEMIND = {'name': 'Battlemind', 'card_type': Ct.power_combat,
              'desc': 'The attack and defense values of this card are equal to'
                      ' the number of cards in your hand after this card is '
                      'played.'}
LATENT_FORCE_ABILITY = {'name': 'Latent Force Ability', 'atk': 7, 'def': 7,
                        'card_type': Ct.power_combat, 'desc': 'Draw 1 card'}

# Special Attack Cards
ANGER = {'name': 'Anger', 'card_type': Ct.power_atk, 'atk': 8,
         'desc': 'After attacking, discard every card in your hand, except one'
                 ' card.'}
ALL_TO_EASY = {'name': 'All To Easy', 'card_type': Ct.power_atk, 'atk': 3,
               'desc': 'If this card is not blocked, the attacked character '
                       'receives 20 damage instead of 3.'}
ASSASSINATION = {'name': 'Assassination', 'card_type': Ct.power_atk, 'atk': 7,
                 'desc': 'After attacking, you may move Zam to an empty space.'
                 }
ATHLETIC_SURGE = {'name': 'Athletic Surge', 'card_type': Ct.power_atk,
                  'atk': 8,
                  'desc': 'After attacking, you may move Darth Maul up to 6 '
                          'spaces.'}
BOWCASTER = {'name': 'Bowcaster Attack', 'card_type': Ct.power_atk, 'atk': 11,
             'desc': 'Draw 1 card.'}
DARK_SIDE_DRAIN = {'name': 'Dark Side Drain', 'card_type': Ct.power_atk,
                   'atk': 3,
                   'desc': 'If Vader does damage to a character with this '
                           'card, Vader recovers the amount of damage done to '
                           'the character.'}
DEADLY_AIM = {'name': 'Deadly Aim', 'card_type': Ct.power_atk, 'atk': 7,
              'desc': 'Draw 2 cards'}
DESPERATE_SHOT = {'name': 'Desperate Shot', 'card_type': Ct.power_atk,
                  'atk': 7,
                  'desc': 'If you do not destroy the defending '
                          'character with this card, destroy Greedo.'}
FORCE_CONTROL = {'name': 'Force Control', 'card_type': Ct.power_atk, 'atk': 7,
                 'desc': 'After attacking, you may move all characters in play'
                         ' up to 3 spaces each.'}
FORCE_STRIKE = {'name': 'Force Strike', 'card_type': Ct.power_atk, 'atk': 6,
                'desc': 'Draw 1 card'}
GAMBLERS_LUCK = {'name': 'Gambler\'s Luck', 'card_type': Ct.power_atk,
                 'atk': 4,
                 'desc': 'If Han does damage with this card, choose an '
                         'opponent to discard a card at random.'}
HEROIC_RETREAT = {'name': 'Heroic Retreat', 'card_type': Ct.power_atk,
                  'atk': 5,
                  'desc': 'After attacking, you may move Han up to 5 spaces.'}
JEDI_ATTACK = {'name': 'Jedi Attack', 'card_type': Ct.power_atk, 'atk': 6,
               'desc': 'After attacking, you may move Obi-wan up to 6 spaces.'}
JUSTICE = {'name': 'Justice', 'card_type': Ct.power_atk, 'atk': 4,
           'desc': 'If Leia has been destroyed, the attack value of this card '
                   'is 10.'}
KYBER_DART = {'name': 'Kyber Dart', 'card_type': Ct.power_atk, 'atk': 9,
              'desc': 'After attacking, if you destroy the defending '
                      'character, draw 3 cards'}
MASTERFUL_FIGHTING = {'name': 'Masterful Fighting', 'card_type': Ct.power_atk,
                      'atk': 5, 'desc': 'Draw 1 card'}
MISSILE_LAUNCH = {'name': 'Missile Launch', 'card_type': Ct.power_atk,
                  'atk': 7,
                  'desc': 'Draw 4 cards'}
PRECISE_SHOT = {'name': 'Precise Shot', 'card_type': Ct.power_atk, 'atk': 9,
                'desc': 'After attacking, you may discard a card to draw a '
                        'card.'}
TAUNTING = {'name': 'Taunting', 'card_type': Ct.power_atk, 'atk': 7,
            'desc': 'Draw 1 card'}
ROCKET_RETREAT = {'name': 'Rocket Retreat', 'card_type': Ct.power_atk,
                  'atk': 4,
                  'desc': 'After attacking, you may move Fett to an empty '
                          'space.'}
SHOT_ON_THE_RUN = {'name': 'Shot On The Run', 'card_type': Ct.power_atk,
                   'atk': 6,
                   'desc': 'After attacking, you may move Padme up to 6 '
                           'spaces.'}
SITH_SPEED = {'name': 'Sith Speed', 'card_type': Ct.power_atk, 'atk': 3,
              'desc': 'Playing this card does not count as an action.'}
SNIPER_SHOT = {'name': 'Sniper Shot', 'card_type': Ct.power_atk, 'atk': 3,
               'desc': 'If this card is not blocked, Zam does 6 damage instead'
                       ' of 3.'}
SUPER_SITH_SPEED = {'name': 'Super Sith Speed', 'card_type': Ct.power_atk,
                    'atk': 4,
                    'desc': 'Playing this card does not count as an action.'}

# Special Defense Cards
BLINDING_SURGE = {
    'name': 'Blinding Surge', 'card_type': Ct.power_def, 'def': 0,
    'desc': 'After taking the attacker\'s damage, Darth Maul does 3 points of '
            'damage to the attacking character.'}
COUNTER_ATTACK = {'name': 'Counterattack', 'card_type': Ct.power_def, 'def': 0,
                  'desc': 'Anakin receives no damage from the attack. '
                          'Instead the attacker receives 1 damage.'}
FORCE_REBOUND = {'name': 'Force Rebound', 'card_type': Ct.power_def, 'def': 0,
                 'desc': 'Yoda receives no damage from the attack. Instead, '
                         'the attacker receives damage equal to the attack '
                         'number on the attacker\'s card.'}
MARTIAL_DEFENSE = {'name': 'Martial Defense', 'card_type': Ct.power_def,
                   'def': 10, 'desc': 'Draw 1 card'}
SERENITY = {'name': 'Serenity', 'card_type': Ct.power_def, 'def': 15,
            'desc': 'Draw 1 card'}
JEDI_BLOCK = {'name': 'Jedi Block', 'card_type': Ct.power_def, 'def': 12,
              'desc': 'Draw 1 card'}
