"""Game states"""

from enum import Enum


class BoardType(Enum):
    """Board Types"""
    geonosis = 0
    freezing_chamber = 1
    kamino = 2
    throne_room = 3


class Main(Enum):
    """Characters"""
    anakin = 0
    han = 1
    luke = 2
    mace = 3
    obi = 4
    yoda = 5
    boba = 6
    dooku = 7
    maul = 8
    vader = 9
    emperor = 10
    jango = 11


class Minor(Enum):
    chewbacca = 12
    greedo = 13
    leia = 14
    padme = 15
    zam = 16
    guard = 17
    trooper = 18
    super_droid = 19
    droid = 20
