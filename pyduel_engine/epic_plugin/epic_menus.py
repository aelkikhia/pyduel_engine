
from pyduel_engine.content.engine_states import SqState

# isolate and replace with generics in engine
from pyduel_engine.content.default_boards import BOARDS
from pyduel_engine.epic_plugin.epic_states import Main


START_MENU = {'question': 'Welcome to Star Wars Epic Duels:',
              'choices': ['Start Game', 'Load Game', 'Play Online']}

CHOOSE_MOVE = {'question': 'Move to Square:', 'choices': None}

CHOOSE_TO_MOVE = {'question': 'Do You Want to Move:', 'choices': ['No', 'Yes']}

CHOOSE_CHARACTER = {'question': 'Choose A Character:', 'choices': None}

CHOOSE_ACTION = {'question': 'Choose an action:',
                 'choices': ['Draw', 'Attack', 'Heal']}

CHOOSE_NUM_PLAYERS = {'question': 'Number of players:', 'choices': range(1, 7)}

CHOOSE_NUM_SQUADS = {'question': 'Number of squads:', 'choices': range(1, 7)}

CHOOSE_SIDE = {'question': 'Choose Side:',
               'choices': [{'type': SqState.light, 'choice': 'Light'},
                           {'type': SqState.dark, 'choice': 'Dark'}]}

CHOOSE_SQUAD = {'question': 'Choose Character:',
                'choices': [{'type': member, 'choice': char}
                            for char, member in Main.__members__.items()]}

CHOOSE_BOARD = {'question': 'Choose Board:',
                'choices': [{'type': board['board_type'],
                             'choice': board['name']}
                            for board in BOARDS.values()]}


if __name__ == '__main__':
    squish = {'question': 'Choose Character:',
              'choices': [{'type': member, 'choice': char}
                          for char, member in Main.__members__.items()]}
    print(squish)
    for char in squish:
        print(char)
