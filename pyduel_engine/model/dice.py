
import logging
import random

from pyduel_engine.content.engine_states import Roll


class Dice(object):

    def __init__(self, sides=5):
        self._sides = sides
        self._result = Roll.two_all

    def __repr__(self):
        return self.__class__.__name__

    def __str__(self):
        return str(self.result.name)

    @property
    def sides(self):
        return self._sides

    @property
    def result(self):
        return self._result

    def print_result(self):
        return Roll(self.result)

    def roll(self):
        self._result = Roll(random.randint(0, self.sides))
        # logging.info("Dice roll: {0}".format(self.result))
        return self.result

    def is_all(self):
        return int(self.result.value) % 2 == 0

    def num(self):
        if self.result == Roll.two_all:
            return 2
        if self.result == Roll.three or self.result == Roll.three_all:
            return 3
        if self.result == Roll.four or self.result == Roll.four_all:
            return 4
        if self.result == Roll.five:
            return 5
