
class Card(object):

    def __init__(self, kwargs):
        self.card_type = kwargs.get('card_type', None)
        self.effects = kwargs.get('effects', [])
        self.atk = kwargs.get('atk', None)
        self.dfs = kwargs.get('dfs', None)
        self.name = kwargs.get('name', None)
        self.owner = kwargs.get('owner', None)
        self.desc = kwargs.get('desc', None)

    def __str__(self):
        return 'Name: {0}, Owner:{0}'.format(self.name, self.owner)

    def __repr__(self):
        return '{0}'.format(self.name)

    def __eq__(self, card):
        return self.card_type == card.card_type

    def has_effects(self):
        return len(self.effects) > 0

    def is_dfs(self):
        return self.dfs is not None

    def is_atk(self):
        return self.atk is not None

    def is_special(self):
        return self.has_effects() and not self.is_dfs() and not self.is_atk()

    def to_json(self):
        return {'card_type': self.card_type, 'name': self.name,
                'atk': self.atk, 'dfs': self.dfs, 'desc': self.desc,
                'owner': self.owner}

    def print_card(self, verbose=False):
        if verbose:
            stats = ''
            if self.name:
                stats += self.name
            if self.atk:
                stats += ' ATK: ' + str(self.atk)
            if self.dfs:
                stats += ' DEF: ' + str(self.dfs)
            if self.desc:
                stats += ' DESC: ' + self.desc
            if self.owner:
                stats += ' Owner: ' + str(self.owner)
            if self.card_type:
                stats += ' TYPE: ' + str(self.card_type)
            return stats
        else:
            return self.name
