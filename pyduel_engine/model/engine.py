import logging
# from random import randrange
# from random import shuffle
from pyduel_engine.model.board import Board
from pyduel_engine.model.dice import Dice


class Engine(object):

    def __init__(self, kwargs):
        """Initializer for Engine"""

        # game object variables
        self._board = Board({'board_type': kwargs.get('board_type', None)})
        self._dice = Dice()
        self._squads = kwargs.get('squads')
        self._num_squads = len(self._squads)

    def __repr__(self):
        return 'Dice: {0}\tSquads: {1}\n{2}'.format(self.dice, self.squads,
                                                    self.board)

    # ###################### Properties ######################################

    @property
    def board(self):
        return self._board

    @property
    def dice(self):
        return self._dice

    @property
    def num_squads(self):
        return self._num_squads

    @property
    def squads(self):
        return self._squads

    ###########################################################################
    #                     Deck / Hand / Discard
    ###########################################################################

    def shuffle_all_decks(self):
        """Shuffle all decks for all squads"""
        for squad in self.squads:
            squad.deck_shuffle()

    def deal_cards_to_squads(self, num_cards):
        """ Deals out hand to each squad Hand defaults to initial 4"""
        for squad in self.squads:
            squad.draw_card(num_cards)

    ###########################################################################
    #                     Movement and Placement
    ###########################################################################

    def place_char(self, char, pos):
        """places a character on the board initially"""
        # change the square state on the board
        self.board.board[pos.x][pos.y]['state'] = char.side
        # give char that position
        char.pos = pos
        logging.info("{0} placed at {1}".format(char.name, pos))

    def move_char(self, char, pos):
        """Move character from one square on the board to the next"""
        self.board.board[pos.x][pos.y]['state'] = char.side
        self.board.make_empty(char.pos)
        char.pos = pos
        logging.info("{0} moved to {1}".format(char.name, pos))

    def remove_char(self, char):
        """Remove character from the board and set hp to zero"""
        # remove from board
        self.board.make_empty(char.pos)
        char.kill()
        logging.info("{0} removed".format(char.name))

    def get_possible_moves(self, char, num_moves=None):
        """Get all possible moves based on the num moves and char"""
        if num_moves is None:
            num_moves = self.dice.num()
        return self.board.find_moves(char, num_moves)

    def get_adj_empty_pos(self, pos):
        """return list of all adjacent empty positions"""
        return self.board.get_adj_empty_pos(pos)

    ###########################################################################
    #                        Character Methods
    ###########################################################################

    # TODO: send character or positions back. Positions are required for
    # the gui, but the char would save time on finding the character again
    def get_char_at_position(self, pos):
        """Get the character at this position. MAYBE ADD MORE LOGIC like:
        checking first to see if active char, that way we just have to sift
        through active squad, otherwise lets look through everyone else. We'll
        see if that is useful or not"""
        for squad in self.squads:
            for char in squad.chars:
                if char.pos == pos:
                    return char

    def get_possible_targets(self, char):
        """ Get list of all possible target characters for a single character
        """
        return [target.pos for squad in self.squads
                if squad.side != char.side
                for target in squad.chars if target.is_alive()
                and self.board.can_target(char, target)]

    def get_adj_chars(self, origin):
        """return list of adj characters. assumes active char unless set"""
        return [char for squad in self.squads
                for char in squad.chars
                if char.is_alive() and origin.pos.is_adj(char.pos)]

    def get_adj_allies_enemies(self, origin):
        """return list of adj friendly and enemy characters"""
        allies = []
        enemies = []
        for squad in self.squads():
            for char in squad.chars:
                if char.is_alive() and origin.pos.is_adj(char.pos):
                    if not origin.is_enemy(char):
                        allies.append(char)
                    else:
                        enemies.append(char)
        return allies, enemies

    ##########################################################################
    #                          JSON Methods
    ##########################################################################

    def squads_json(self):
        return [squad.to_json() for squad in self.squads]


if __name__ == '__main__':
    from pyduel_engine.content.engine_states import SqState, BoardType
    from pyduel_engine.utilities import squad_utilities as s_utils
    from pyduel_engine.epic_plugin.epic_states import Main

    # load squads
    squads = [s_utils.setup_squad(1, Main.dooku, SqState.dark),
              s_utils.setup_squad(2, Main.mace, SqState.light)]
    board_type = BoardType.ruins
    engine = Engine({'squads': squads, 'board_type': board_type})
    print(engine)
    print(engine.dice.num())
