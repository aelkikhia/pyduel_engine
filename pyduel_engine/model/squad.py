
from random import randrange
from random import shuffle

# Main_char_index
MAIN = 0


class Squad(object):
    """ Squad class keeps track of a squad of characters """

    def __init__(self, kwargs):
        self._active_card = kwargs.get('active_card', None)
        self._can_draw = kwargs.get('can_draw', True)
        self._chars = kwargs.get('chars', [])
        self._deck = kwargs.get('deck', [])
        self._discard = kwargs.get('discard', [])
        self._hand = kwargs.get('hand', [])
        self._num_actions = kwargs.get('actions', 2)
        self._player_num = kwargs.get('player_num')
        self._side = kwargs.get('side')

    def __repr__(self):
        return '\nPlayer: {0}\tSide: {1}\tMain: {2}\tActions: {3}\tHand: {4}'\
            .format(self.player_num, self.side, self.chars[MAIN].__repr__(),
                    self.num_actions, self.hand)

    def __str__(self):
        return '\nPlayer: {0}\tSide: {1}\tMain: {2}\tActions: {3}\tHand: {4}'\
            .format(self.player_num, self.side, self.chars[MAIN].__str__(),
                    self.num_actions, self.hand)

# ##################### Properties ###############################

    @property
    def active_card(self):
        return self._active_card

    @property
    def can_draw(self):
        return self._can_draw

    @property
    def chars(self):
        return self._chars

    @property
    def deck(self):
        return self._deck

    @property
    def discard(self):
        return self._discard

    @property
    def hand(self):
        return self._hand

    @property
    def num_actions(self):
        return self._num_actions

    @property
    def player_num(self):
        return self._player_num

    @property
    def side(self):
        return self._side

# ########################## Actions Rules ###################################

    def add_actions(self, num_actions):
        """add/sub actions"""
        self._num_actions += num_actions

    def has_action(self):
        """can squad perform any actions"""
        return self.num_actions > 0

    def can_draw_card(self):
        """check if squad can act and if they are able to draw a card"""
        return self.has_action() and self.can_draw

    def can_play_card(self):
        """can squad play card"""
        return self.has_action() and self.has_hand()

    def can_heal_main(self):
        """Can heal minor character. main must be dead and must have main card
        """
        if self.has_action():
            return self.chars[MAIN].is_damaged() and \
                self.are_minors_dead() and self.has_minor_card()
        return False

    def can_heal_minor(self):
        if self.is_main_dead() and self.has_main_card():
            for char in self.chars[1:]:
                return char.is_damaged()

# ##################### Character ######################################

    def is_main_dead(self):
        """is main character dead """
        return self.chars[MAIN].hp == 0

    def are_minors_dead(self):
        """is/are minor character(s) dead """
        for char in self.chars[1:]:
            if char.is_alive():
                return False
        return True

# ########################### Deck ######################################

    def get_card_from_deck(self, card_type):
        """Get card from deck and put it in hand then shuffles deck """
        self.hand.append(self.deck.pop(self.deck.index(card_type)))
        shuffle(self.deck)

    def deck_preview(self, depth):
        """Show the cards from index 0 to depth """
        return self.deck[:depth]

    def deck_reorder(self, indices):
        """Reorder deck """
        reorder = [self.deck[i] for i in indices]
        self.deck[0:len(indices)] = reorder

    def deck_shuffle(self):
        """Shuffles the deck """
        return shuffle(self.deck)

# ########################### Discard ###################################

    def discard_find_card(self, card_type):
        """find specific card in discard pile and return index """
        pass

    def get_card_from_discard(self, card_type):
        """Take a card from discard and put it in hand """
        self.hand.append(self.discard.pop(self.discard.index(card_type)))
        shuffle(self.discard)

    def list_discard(self):
        """list all card types in discard """
        return list(set(self.discard))

# ########################### Hand ######################################

    def set_active_card(self, card_index):
        self._active_card = card_index

    def discard_hand(self):
        """puts hand in discard pile """
        self.discard.extend(self.hand)
        self._hand = []

    def get_card_from_hand(self):
        return self._hand[self._active_card]

    def has_hand(self):
        """Does squad have any cards in hand return boolean """
        # TODO: checking length might be slower than checking if has element
        return len(self.hand) > 0

    def has_main_card(self):
        """check hand for card belonging to main character """
        if self.has_hand():
            for card in self.hand:
                if card.owner == self.chars[MAIN].type:
                    return True
        return False

    def has_minor_card(self):
        """check hand for card belonging to minor character """
        if self.has_hand():
            for card in self.hand:
                if card.owner != self.chars[MAIN].type:
                    return True
        return False

# ###################### Card Utilities #################################

    def action_play_card(self):
        """discard active card, decrement action, set active card to None """
        print("in action play card")
        self.discard_card(self._active_card)
        self.add_actions(-1)
        self._active_card = None

    def action_draw(self):
        """Draw card and decrement action """
        self.draw_card()
        self.add_actions(-1)

    def draw_card(self, num_cards=1):
        """ draw card. if num_cards specified draw more """
        for x in range(0, num_cards):
            self.hand.append(self.deck.pop())

    def discard_card(self, card_index=None):
        """Discard a card. if card_index is None, discard random card """
        if not card_index:
            card_index = randrange(0, len(self.hand))

        self.discard.append(self.hand.pop(card_index))

    def discard_cards(self, num_cards=None, card_indices=None):
        """discard multiple cards"""
        if num_cards:
            for _ in range(0, num_cards):
                self.discard_card()
        if card_indices:
            for index in sorted(card_indices, reverse=True):
                self.discard_card(index)
        else:
            # todo: throw an exception
            pass

    def list_character_cards(self):
        """check hand for card indexes belonging to main/minor characters """
        main = []
        minor = []
        if self.has_hand():
            for x in range(0, len(self.hand)):
                if self.hand[x].owner == self.chars[MAIN].type:
                    main.append(x)
                else:
                    minor.append(x)
        return main, minor

    def shuffle_discard_into_deck(self):
        """shuffle discard into the deck """
        self.deck.extend(self.discard)
        self._discard = []
        shuffle(self.deck)

# ###################### Squad Utilities #################################

    def get_active_chars(self):
        """ Returns a list of active character keys ex: ['main', 'minor1'] """
        return [i for i, char in enumerate(self.chars) if char.is_alive()]

    def set_squad_side(self, side):
        """Set what side the squad """
        self._side = side
        for char in self.chars:
            char.side = side

# ####################### Print / Format Methods ############################

    def to_json(self):
        chars = [char.to_json() for char in self.chars]
        return {'player': self.player_num, 'actions': self.num_actions,
                'side': self.side, 'Deck': self.deck, 'hand': self.hand,
                'discard': self.discard, 'chars': chars}

    def print_deck(self, verbose=False):
        for card in self.deck:
            card.print_card(verbose)

    def print_hand(self, verbose=False):
        for card in self.hand:
            card.print_card(verbose)

    def print_discard(self, verbose=False):
        for card in self.hand:
            card.print_card(verbose)
