
class Character(object):

    def __init__(self, kwargs):
        self._deck = kwargs.get('deck', [])
        self._max_hp = kwargs.get('max_hp', 0)
        self._hp = kwargs.get('hp', self._max_hp)
        self._init_pos = kwargs.get('init_pos', None)
        self._is_range = kwargs.get('is_range', False)
        self._is_down = kwargs.get('is_down', False)
        self._name = kwargs.get('name')
        self._pos = kwargs.get('pos', None)
        self._side = kwargs.get('side', None)
        self._type = kwargs.get('type')

    def __repr__(self):
        return 'Name: {0}\tHP: {1}\tPos: {2}'.format(
            self.name, self.hp, self.pos)

    def __str__(self):
        """String method for this object """
        return 'Name: {0}\tHP: {1}\tIs Range: {2}\tSide: {3}\tPos: {4}'.format(
            self.name, self.hp, self.is_range, self.side, self.pos)

    # ############################ Properties #################################

    @property
    def deck(self):
        """The Cards for this character """
        return self._deck

    @deck.setter
    def deck(self, deck):
        self._deck = deck

    @property
    def max_hp(self):
        """Maximum Health """
        return self._max_hp

    @property
    def hp(self):
        """Character Health """
        return self._hp

    @property
    def init_pos(self):
        """Initial position for character on the generic boards """
        return self._init_pos

    @property
    def is_range(self):
        """Is this character a ranged character """
        return self._is_range

    @property
    def is_down(self):
        """Is this character unable to move or act """
        return self._is_down

    @property
    def name(self):
        """Character name """
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def pos(self):
        """Character pos """
        return self._pos

    @pos.setter
    def pos(self, pos):
        self._pos = pos

    @property
    def side(self):
        """Characters side """
        return self._side

    @side.setter
    def side(self, side):
        self._side = side

    @property
    def type(self):
        """Character type """
        return self._type

    # ############################### Modifiers ###############################

    def kill(self):
        """Sets hp to zero and position on board to none """
        self._hp = 0
        self._pos = None

    def damage(self, points):
        """Subtracts points of health. If it is 0 or negative
        kill the character and remove it from the board """
        if self._hp - points < 1:
            self.kill()
        else:
            self._hp -= points

    def heal(self, points=1):
        """Add points of health up to maximum health points """
        if self._hp + points > self.max_hp:
            self._hp = self.max_hp
        else:
            self._hp += points

    # ############################### Discovery ###############################

    def is_alive(self):
        """Is this character Alive """
        return self._hp > 0

    def is_damaged(self):
        """No point in healing if you are already at max hp """
        return 0 < self._hp < self.max_hp

    def is_enemy(self, char):
        """Is this an enemy character """
        return self.side != char.side

    def is_adj(self, char):
        """Is this character adjacent """
        return self.pos.is_adj(char.pos)

    def is_diagonal(self, char):
        """Is this character diagonal """
        return self.pos.is_diagonal(char.pos)

    def is_parallel(self, char):
        """Is this character parallel """
        return self.pos.is_parallel(char.pos)

    # ############################### JSON ###############################

    def to_json(self):
        """export the character as a dictionary/json"""
        return {'name': self.name, 'max_hp': self.max_hp, 'hp': self._hp,
                'is_range': self.is_range, 'side': self.side, 'pos': self.pos}
