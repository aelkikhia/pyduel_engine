import logging
# from random import randrange
from random import shuffle
from pyduel_engine.content.engine_states import SqState as Sqr
# from pyduel_engine.content.engine_states import Action as Act
from pyduel_engine.model.engine import Engine


STARTING_HAND_SIZE = 4
MAX_HAND_SIZE = 10


class Game(object):

    def __init__(self, kwargs):
        """Initializer for Engine"""

        # Player variables
        self.num_players = kwargs.get('num_players', 0)

        # game object variables
        self.engine = Engine(kwargs)
        # Game stage variables

        # TODO: either move or action phase... maybe combat phase in which case
        self.move_phase = ('move_phase', False)

        self.turn = kwargs.get('turn', 0)
        self.round = kwargs.get('round', 0)

        # phase variables
        self.active_char = kwargs.get('active_char', None)
        self.active_target = kwargs.get('active_target', None)
        self.active_action = kwargs.get('action', None)
        self.possible_targets = kwargs.get('target_squares', None)
        self.possible_cards = kwargs.get('possible_cards', None)
        self.targets = kwargs.get('targets', None)
        self.num_moves = 0
        self.game_event = None

        # {char_index: list playable card}
        self.selectedCard = None
        self.playable_cards = None

    def __repr__(self):
        return 'Round: {0}\tTurn: {1}\nEngine:{2}'.format(
            self.round, self.turn, self.engine)

    ##########################################################################
    # Accessors
    ##########################################################################
    def active_card(self):
        """active card getter """
        return self.active_squad().active_card

    def set_active_card(self, card_index=None):
        """active card setter """
        self.active_squad().set_active_card(card_index)

    def active_squad(self):
        """Returns the active squad """
        return self.engine.squads[self.turn]

    def board(self):
        """Returns the engine board object """
        return self.engine.board

    def dice(self):
        """Returns the engine board object """
        return self.engine.dice

    def get_char_at_position(self, pos):
        """returns character at position """
        return self.engine.get_char_at_position(pos)

    def get_active_chars(self):
        """returns a list of positions of the active characters """
        pos_list = list()
        active = self.active_squad().get_active_chars()
        for key in active:
            pos_list.append(self.active_squad().chars[key].pos)
        return pos_list

    def is_move_phase(self):
        """Is move phase """
        return self.move_phase

    def num_squads(self):
        """Returns num of squads """
        return self.engine.num_squads

    def squads(self):
        """Returns the engine board object """
        return self.engine.squads

    def target_squad(self):
        """Returns the target squad """
        return self.target_squad

    ##########################################################################
    #                                Modifiers
    ##########################################################################

    def add_num_squad_actions(self, num_actions=2):
        """Gives the active squad actions """
        self.active_squad().add_actions(num_actions)

    def increment_turn(self):
        """Increments the turn """
        self.game_event = "{0} Turn End".format(self.print_status())
        self.turn += 1

    def increment_round(self):
        """Increment round and resets the turn """
        self.game_event = "{0} Round End".format(self.print_status())
        self.turn = 0
        self.round += 1

    def move_char(self, char, pos):
        """moves char on the board, clear the active char"""
        self.game_event = "{0} moved to {1} -> {2}".format(
            char.name, char.pos, pos)
        self.engine.move_char(char, pos)
        self.num_moves -= 1

    def remove_char(self, char):
        """removes char from the board """
        self.engine.remove_char(char)

    def reset_active_squad_cards(self):
        """Resets the playable cards and active card """
        self.active_card_select(None)
        self.playable_cards = None

    def roll_dice(self):
        """Roll dice """
        self.dice().roll()
        self.game_event = "Roll {0}".format(self.dice().print_result())

    def active_card_select(self, card_index):
        """Sets the active card to card_index or sets it to none """
        if self.active_squad().active_card is None:
            self.active_squad().active_card = card_index
        else:
            self.active_squad().active_card = None

    def active_char_select(self, pos):
        """set the active char based on pos or by direct char select """
        self.active_char = self.engine.get_char_at_position(pos)
        self.set_possible_targets()
        if not self.move_phase:
            self.set_playable_cards()

    def set_possible_targets(self):
        """set the possible targets """
        if self.move_phase:
            self.possible_targets = self.get_possible_moves()
        else:
            self.possible_targets = self.get_possible_targets()

    def set_playable_cards(self):
        self.playable_cards = self.active_chars_playable_cards()

    def set_active_target(self, pos):
        self.active_target = self.engine.get_char_at_position(pos)

    def set_num_movable_chars(self):
        """Set (int) number of moves for squad based on dice roll """
        if self.dice().is_all():
            self.num_moves = len(self.active_squad().get_active_chars())
        else:
            self.num_moves = 1

    ##########################################################################
    #                          Setup functions
    ##########################################################################

    def setup(self):
        """Initializes the game:
            - shuffles decks
            - and deals cards to squads
            - places characters on the board """
        self.game_event = 'setting up game'
        self.place_main_characters()
        # self.game.randomly_place_minor_characters()
        # TODO: create initial placement phase
        # make the main character active char
        # self.active_char = self.active_squad().chars[0].pos
        # get the adjacent empty positions for minor character placement
        # self.targets = self.get_adj_empty_pos(self.active_char)
        # select squares for minor character placement

        # shuffle all decks
        self.shuffle_all_decks()
        # draw default hand
        self.deal_cards_to_squads()

    def randomly_place_minor_characters(self):
        """places minor characters at random adjacent positions """
        # place secondary characters
        for squad in self.engine.squads:
            # get list of empty adjacent positions
            positions = self.engine.board.get_adj_empty_pos(squad.chars[0].pos)
            # shuffle the positions
            shuffle(positions)
            # hard coding the first two positions for the minor chars
            self.char_place(squad.chars[1], positions[0])
            self.char_place(squad.chars[2], positions[1])

    def place_main_characters(self):
        """Initial placement of main characters on the board based on their
        predefined board positions """
        logging.info('Placing Main Characters on Board')
        board = self.engine.board.type
        for squad in self.engine.squads:
            pos = squad.chars[0].init_pos[board]
            self.engine.place_char(squad.chars[0], pos)

    def char_place(self, char, pos):
        """places char on the board """
        self.game_event = "{0} placed at {1}".format(char.name, pos)
        self.engine.place_char(char, pos)

    ##########################################################################
    #                        Game Phases
    ##########################################################################

    def move_phase_start(self):
        """Move phase setup """
        self.game_event = "{0} Move phase start".format(self.print_status())
        self.move_phase = True
        self.roll_dice()
        self.set_num_movable_chars()

    def move_phase_end(self):
        """Move phase cleanup """
        self.game_event = "{0} Move phase end".format(self.print_status())
        # self.active_char = None
        # self.targets = None
        self.num_moves = 0

    def action_phase_start(self):
        """Action phase setup """
        self.game_event = "{0} Action phase start".format(self.print_status())
        self.move_phase = False

    def action_phase_end(self):
        """Action phase cleanup """
        self.game_event = "{0} Action phase end".format(self.print_status())
        self.add_num_squad_actions()
        self.active_char = None
        self.targets = None

    ##########################################################################
    #                        Game Rules
    ##########################################################################

    def is_game_over(self):
        """returns true if all main dark or light side characters are dead """
        return (sum(squad.chars[0].hp for squad in self.engine.squads
                    if squad.side == Sqr.light) == 0) or \
               (sum(squad.chars[0].hp for squad in self.engine.squads
                    if squad.side == Sqr.dark) == 0)

    # TODO : decrement number of squads as they are defeated

    # def is_legal_target(self, char, target):
    #     """verify if legal target"""
    #     if not char.is_range and not char.is_adj(target):
    #         return False
    #     return char.side != target.side

    # def can_melee_attack(self, char):
    #     """verify if target can be melee attacked"""
    #     return self.active_char().is_legal_target(char) and self.is_adj(char)

    ###########################################################################
    #                        Discovery Methods
    ###########################################################################

    def can_play_card(self):
        return self.active_char is not None and \
            self.active_card() is not None and self.active_target is not None

    def get_playable_cards(self):
        """returns list of card indices if the active character can use them
        {char_index: [card_indices]} """
        # TODO: might want to do this outside the scope of this function
        # Card related Actions
        playable_cards = {}
        for i, char in enumerate(self.active_squad().chars):
            playable_cards[i] = []
            for j, card in enumerate(self.active_squad().hand):
                # get playable card index if it matches a char
                if card.owner == char.type:
                    # TODO: maybe add flag for target required
                    # if its an attack card or special and
                    # doesn't need target
                    if card.is_atk() or card.is_special():
                        playable_cards[i].append(j)
        # TODO: maybe make this into a chain of methods
        return playable_cards

    def active_chars_playable_cards(self, char=None):
        """returns list of card indices if the active character can use them
        {char_index: [card_indices]} """
        # TODO: might want to do this outside the scope of this function
        # Card related Actions
        if char is None:
            char = self.active_char
        playable_cards = []
        for i, card in enumerate(self.active_squad().hand):
            # get playable card index if it matches a char
            if card.owner == char.type:
                if card.is_atk() or card.is_special():
                    playable_cards.append(i)
        # TODO: maybe make this into a chain of methods
        return playable_cards

    def get_possible_moves(self, char=None, num_moves=None):
        """Get all possible moves based on the num moves and char """
        if char is None:
            char = self.active_char
        return self.engine.get_possible_moves(char, num_moves)

    # TODO: Clean this up
    # def get_list_of_char_card_actions(self):
    #     active_chars = self.active_squad().get_active_chars()

    def get_squad_targets(self):
        """ Get dictionary of lists for all possible targets
        {char_index: {cards:[] {targets: [pos]} """
        # TODO: MIGHT BE excessive
        active_chars = self.active_squad().get_active_chars()
        squad_targets = {}
        for index, char in enumerate(active_chars):
            targets = self.get_possible_targets(
                self.active_squad().chars[index])
            if targets:
                squad_targets[index] = targets
        return squad_targets

    def get_adj_empty_pos(self, pos):
        return self.engine.board.get_adj_empty_pos(pos)

    # ######################### Targeting Methods ############################

    def get_possible_targets(self, char=None):
        """ Get list of all possible target characters for a single character
        """
        if char is None:
            char = self.active_char
        return self.engine.get_possible_targets(char)

    def get_adj_chars(self, char):
        """return list of adj characters. assumes active char unless set """
        if char is None:
            char = self.active_char
        return self.engine.get_adj_chars(char)

    def get_adj_allies_enemies(self, char):
        """returns lists of adj friendly and enemy characters """
        return self.engine.get_adj_allies_enemies(char)

    def get_minor_enemy_chars(self, char=None):
        enemies = []
        if char:
            active_char = char
        else:
            active_char = self.active_char

        for squad in self.engine.squads:
            if squad.side != active_char.side:
                enemies.append(squad.get_minor_chars())
        return enemies

    ##########################################################################
    #                         Action Methods
    ##########################################################################

    def action_play_card(self):
        print(self.active_char)
        print(self.active_card())
        print(self.active_target)
        self._play_card()
        self.active_squad().action_play_card()
        print(self.active_squad())

    def action_draw(self):
        self.game_event = "{0} Card Draw".format(self.print_status())
        self.active_squad().action_draw()

    def _play_card(self):
        """Game play card set up """
        card = self.active_squad().get_card_from_hand()
        if card.is_atk():
            self.game_event = "{0} attacks {1}".format(self.active_char.name,
                                                       self.active_target.name)
            self.damage_char(self.active_target, card.atk)
            # TODO: ask target owner if they want to defend
            # TODO: resolve cards

        if card.has_effects():
            print("card had effects")
            for effect in card.effects:
                print(effect)

    ###########################################################################
    # ############################# Game Effects ##############################
    ###########################################################################

    # ##################### Hand / Deck / Discard Effects #####################

    def shuffle_all_decks(self):
        """Shuffle all decks for all squads """
        self.engine.shuffle_all_decks()

    def deal_cards_to_squads(self, num_cards=STARTING_HAND_SIZE):
        """ Deals out hand to each squad Hand defaults to initial 4 """
        self.engine.deal_cards_to_squads(num_cards)

    # ############################# Move Effects ##############################
    #
    # def switch_main_and_minor_pos(self, char):
    #     """Main character switches position with their minor character"""
    #     self.active_char.switch_pos(char)
    #
    # def move(self, num_moves):
    #     """Shot on the run, Heroic Retreat, Jedi Attack, Athletic Surge"""
    #     self.board().find_moves(self.active_char, num_moves)
    #
    # ############################# Damage Effects ############################
    # TODO: Reevaluate all - pass in list of chars, chars, or position of chars
    #
    # def damage_adj_chars(self, char, points=1):
    #     """Damages all adjacent characters. No target required"""
    #     # TODO: pass in a list of char positions
    #     chars = self.get_adj_chars(char)
    #     for char in chars:
    #         char.damage(points)
    #
    # def damage_adj_enemies(self, points):
    #     allies, enemies = self.get_adj_allies_enemies(self.active_char)
    #     self.damage_chars(points, enemies)
    #
    def damage_char(self, char, points=1):
        """Direct damage to char """
        char.damage(points)
    #
    # def damage_minor_enemy(self, points, char):
    #     """Choke"""
    #     char.damage(points)
    #
    # def damage_char_and_adj_chars(self, char, points=1):
    #     """Thermal Detonator"""
    #     char.damage(points)
    #     self.damage_adj_chars(char, points)
    #
    # def draw(self, num):
    #     """Jedi block, martial defense, serenity, missile launch, taunt,
    #     masterful fighting, force strike, deadly aim, bowcaster attack,
    #     Latent Force Ability, gain power"""
    #     self.active_squad().draw_card(num)
    #
    # def reveal_discard_attack_cards(self, squad_num):
    #     """I will not fight you"""
    #     self.active_squad().discard_attack_cards()
    #     self.squads()[squad_num].discard_attack_cards()
    #
    # def reveal_target_hand(self, squad_num):
    #     """"""
    #     # TODO: FIX THIS
    #     return self.squads()[squad_num].hand
    #
    # def reveal_target_hand_discard_card(self, squad_num, card_indices):
    #     """Insight"""
    #     self.reveal_target_hand(squad_num)
    #     self.target_discard(squad_num, card_indices)
    #
    # def squad_cannot_draw_next_turn(self):
    #     pass
    #
    # def no_hand_draw(self, num_cards):
    #     """if active player has no hand, then draw cards"""
    #     if not self.active_squad().has_hand():
    #         self.draw(num_cards)
    #
    # ##########################  Action effects ##############################
    #
    # def add_action(self, squad_num):
    #     """squad num gains action"""
    #     self.squads()[squad_num] += 1
    #
    # def lose_action(self, squad_num):
    #     """squad num loses an action"""
    #     self.squads()[squad_num] -= 1
    #
    # ####################### Combination Effects ############################
    #
    # def damage_and_discard_random(self, points, squad_num, num_cards):
    #     """Force Lightning"""
    #     self.damage_char(points)
    #     self.target_discard_random(squad_num, num_cards)
    #
    # def damage_legal_targets_shuffle_discard_into_deck(self, points):
    #     """Deal damage to all possible legal targets and shuffle discard pile
    #     into the deck"""
    #     self.damage_legal_targets(points)
    #     self.active_squad().shuffle_discard_into_deck()
    #
    # def heal_main_if_adjacent_alive_or_heal_minor(self, points):
    #     # """Luke's in trouble"""
    #     pass
    #
    # def heal_more_if_main_alive(self, alive_pts, dead_pts):
    #     """Protection"""
    #     if self.active_squad().is_main_dead():
    #         self.active_char.heal(dead_pts)
    #     else:
    #         self.active_char.heal(alive_pts)
    #
    # def heal_target_squad_cannot_draw_next_turn(self, points):
    #     """Meditation"""
    #     self.heal(points)
    #     self.squad_cannot_draw_next_turn()
    #
    # def heal(self, points=None):
    #     self.active_char().heal(points)
    #
    # def heal_and_move(self, points, moves):
    #     """Wookie Healing"""
    #     self.heal(points)
    #     self.move(moves)
    #
    # def move_and_damage_target_char(self, char, pos, points):
    #     """move a target and deal damage to the target"""
    #     self.move_char(char, pos)
    #     char.damage(points)
    #
    # def move_and_damage_adj_chars(self, points):
    #     """Flame Thrower"""
    #     # TODO: FIX
    #     chars = self.get_adj_chars(self.active_char)
    #     # for char in chars:
    #     #     self.move_character(char)
    #     #     char.damage(points)
    #
    # def move_and_draw(self, moves, num_cards):
    #     """wisdom, force quickness"""
    #     self.move(moves)
    #     self.draw(num_cards)
    #
    # def counter(self, points):
    #     """Counter attack, blinding surge, force rebound"""
    #     self.active_char().damage(points)
    #
    # def move_squad_draw(self, num_moves, num_cards):
    #     """Children of the force"""
    #     self.move_squad(num_moves)
    #     self.draw(num_cards)
    #
    # def move_no_hand_draw_new_hand(self, num_moves, num_cards):
    #     """Calm"""
    #     self.move(num_moves)
    #     self.no_hand_draw(num_cards)
    #
    # def discard_to_draw(self):
    #     """precise shot"""
    #     self.active_squad().discard_to_draw()
    #
    # def target_dies_draw(self, cards):
    #     """kyber dart"""
    #     if self.target_char.hp == 0:
    #         self.draw(cards)
    #
    # def minor_dead_more_damage(self):
    #     """Justice"""
    #     if self.active_squad().can_heal_main():
    #         pass
    #
    # def damage_char_squad_loses_action(self, points, action):
    #     """wrist cable"""
    #     self.target_char.damage(points)
    #     # TODO: FIX THIS
    #     self.lose_action(action)
    #
    # def damage_squad(self, points):
    #     """wrath_vader"""
    #     self.target_squad.damage(points)
    #
    # def damage_legal_targets(self, points):
    #     """Never tell me the odds, whirlwind"""
    #     targets = self.get_possible_targets(self.active_char)
    #     for target in targets:
    #         target.damage(points)
    #
    # def target_discard(self, squad_num, card_indices):
    #     """Let Go of your Hatred"""
    #     self.squads()[squad_num].discard_cards(card_indices=card_indices)
    #
    # def target_discard_random(self, squad_num, num_cards):
    #     """Force Drain"""
    #     self.squads()[squad_num].discard_cards(num_cards=num_cards)
    #
    # def damage_legal_target_discard_random(self, squad_num, num_cards):
    #     """Gambler's luck"""
    #     self.target_discard_random(squad_num, num_cards)
    #
    # def move_all(self, moves):
    #     # force control
    #     pass
    #
    # def lift_char(self):
    #     # Force Lift
    #     pass
    #
    # def drop_char(self):
    #     pass
    #
    # def reorder_cards_draw(self, card_indices, num_draw):
    #     """Future Foreseen"""
    #     self.reorder_cards(card_indices)
    #     self.draw(num_draw)
    #
    # def preview_deck(self, depth):
    #     self.active_squad().deck_preview(depth)
    #
    # def reorder_cards(self, card_indices):
    #     self.active_squad().deck_reorder(card_indices)
    #
    # def kill_or_die(self, char):
    #     """Desperate Shot"""
    #     if char.is_alive:
    #         self.active_char().kill()
    #
    # def hand_size_to_one(self, card_index):
    #     """anger"""
    #     self.active_squad().hand_size_to_one(card_index)
    #
    # def drain_hp(self, points):
    #     # """dark side drain"""
    #     pass
    #
    # def hand_size_combat_modifier(self):
    #     # """battlemind"""
    #     pass
    #
    # def get_card_from_deck(self, card_type):
    #     """Wookie instincts"""
    #     self.active_squad().get_card_from_deck(card_type)
    #     self.active_squad().shuffle()
    #
    # def get_card_from_discard(self, card_type):
    #     """jedi mind trick"""
    #     self.active_squad().get_card_from_discard(card_type)
    #
    # def squad_discard_hand(self, squad_num):
    #     """You Will Die"""
    #     self.squads()[squad_num].discard_hand()
    #
    # def all_squads_discard_hand(self):
    #     for squad in self.squads:
    #         squad.discard_hand()
    #
    # def squads_discard_redraw(self, num_cards):
    #     """Force Balance"""
    #     for squad in self.squads():
    #         squad.discard_hand()
    #         squad.draw_card(num_cards)
    #
    # def target_squad_reveal_hand(self):
    #     # TODO: Not sure on exposure
    #     pass
    #
    # def target_squad_discard_special(self):
    #     """Your Skills Are Not Complete"""
    #     self.target_squad_reveal_hand()
    #     pass
    #
    # def teleport(self, pos, char=None):
    #     """rocket retreat, assassination"""
    #     if char:
    #         self.move_char(char, pos)
    #     else:
    #         self.move_char(self.active_char, pos)
    #
    # def teleport_damage_damage(self, pos, points):
    #     """Force Push"""
    #     self.teleport(pos, self.target_char)
    #     self.target_char.damage(points)
    #
    # def teleport_adj_to_enemy(self):
    #     pass
    #
    # def teleport_not_action(self, pos, char=None):
    #     """Fire up the jet pack"""
    #     self.teleport(pos, char)
    #     self.not_action()
    #
    # def teleport_to_minor_damage(self, points):
    #     """wrath_anakin"""
    #     self.teleport_adj_to_enemy()
    #     self.target_char.damage(points)
    #
    # def teleport_to_target_not_action(self):
    #     """sudden arrival"""
    #     self.teleport_adj_to_enemy()
    #     self.not_action()
    #
    # def undefended_attack(self):
    #     """all to easy, sniper shot"""
    #     self.active_squad()
    #     pass

    ##########################################################################
    #                          JSON Methods
    ##########################################################################
    def squads_json(self):
        """returns sequential list of squads in json """
        return self.engine.squads_json()

    ##########################################################################
    #                          Print Methods
    ##########################################################################

    def print_status(self):
        """Prints the round and turn """
        return "R:{0} T:{1}".format(self.round + 1, self.turn + 1)

if __name__ == '__main__':
    from pyduel_engine.content.engine_states import SqState, BoardType
    from pyduel_engine.utilities import squad_utilities as s_utils
    from pyduel_engine.epic_plugin.epic_states import Main

    # load squads
    squads = [s_utils.setup_squad(1, Main.dooku, SqState.dark),
              s_utils.setup_squad(2, Main.mace, SqState.light)]
    board_type = BoardType.ruins
    game = Game({'squads': squads, 'board_type': board_type})
    game.num_players = 2
    print(game)

    game.active_char = squads[0].chars[0]

    movements = game.get_possible_moves()
    print(movements)
    print(game.engine.dice.num())
