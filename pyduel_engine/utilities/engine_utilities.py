
import time
import logging
from functools import wraps, partial


def time_this(func):
    """
    decorator function timer
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(func.__name__, end-start)
        return result
    return wrapper


def logger(func=None, *, level=logging.INFO, name=None, message=None):
    if func is None:
        return partial(logger, level=level, name=name, message=message)

    log_name = name if name else func.__module__
    log = logging.getLogger(log_name)
    log_msg = message if message else func.__name__

    @wraps(func)
    def wrapper(*args, **kwargs):
        log.log(level, log_msg)
        return func(*args, **kwargs)
    return wrapper
