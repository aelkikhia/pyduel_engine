
from pyduel_engine.epic_plugin import epic_menus as menus
from pyduel_engine.model.menu import MenuLoader


def choose_to_move():
    """
    chose to move from current position to new position
    """
    move_menu = menus.CHOOSE_TO_MOVE
    menu = MenuLoader(move_menu)
    choice = menu.load_menu()
    return choice


def choose_move(moves):
    """
    chose move from current position to new position
    """
    move_menu = menus.CHOOSE_MOVE
    move_menu['choices'] = moves

    menu = MenuLoader(move_menu)
    choice = menu.load_menu()
    return choice


def choose_character(characters):
    """
    choose character then uses returned action type as key to dictionary
    of action methods
    """
    move_menu = menus.CHOOSE_CHARACTER
    move_menu['choices'] = characters

    menu = MenuLoader(move_menu)
    choice = menu.load_menu()
    return choice


def choose_actions(actions):
    """
    calls choose action then uses returned action type as key to dictionary
    of action methods
    """
    move_menu = menus.CHOOSE_ACTION
    move_menu['choices'] = actions

    menu = MenuLoader(move_menu)
    choice = menu.load_menu()
    return choice


def choose_number_of_players(choice=None):
    """Text choose number of players """
    if not choice:
        menu = MenuLoader(menus.CHOOSE_NUM_PLAYERS)
        return menu.load_menu()
    return choice


def choose_player(player, choice=None):
    """Text choose number of players """
    if not choice:
        menu = MenuLoader({'question': 'Choose Player:',
                           'choices': range(1, player + 1)})
        return menu.load_menu()
    return choice


def choose_number_of_squads(choice=None):
    """Text choose number of players """
    if not choice:
        menu = MenuLoader(menus.CHOOSE_NUM_SQUADS)
        return menu.load_menu()
    return choice


def choose_player_sides(choice=None):
    if not choice:
        menu = MenuLoader(menus.CHOOSE_SIDE)
        return menu.load_enum_menu()
    return choice


def choose_players_squads(choice=None):
    if not choice:
        menu = MenuLoader(menus.CHOOSE_SQUAD)
        return menu.load_enum_menu()

    return choice


def choose_game_board(choice=None):
    if not choice:
        menu = MenuLoader(menus.CHOOSE_BOARD)
        choice = menu.load_enum_menu()
    return choice
