
from pyduel_engine.content.default_squads import SQUADS
from pyduel_engine.model.squad import Squad

###############################################################################
#                   Squad and Character Setup Methods
###############################################################################


def create_deck(cards, owner):
    for card in cards:
        card.owner = owner
        card.print_card(True)
    return cards


def character_initializer(char, side):
    char.side = side
    char.deck = create_deck(char.deck, char.type)
    return char


def setup_squad(player_number, char_type, side):
    """Builds squads"""
    chars = SQUADS[char_type]
    for index, char in enumerate(chars):
        character_initializer(char, side)
        if index > 0:
            char.name = "{0} {1}".format(char.name, index)
    squad = Squad({'player_num': player_number, 'side': side, 'chars': chars,
                   'deck': chars[0].deck + chars[1].deck})

    return squad

if __name__ == '__main__':
    from pyduel_engine.content.engine_states import SqState
    from pyduel_engine.epic_plugin.epic_states import Main
    test = setup_squad(1, Main.mace, SqState.dark)
    print(test.json_format())
    print(test.chars[0])
    print(test.chars[1])
    print(test.chars[2])
