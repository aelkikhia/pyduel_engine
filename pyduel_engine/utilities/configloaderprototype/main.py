#!/usr/bin/python
# -*- coding: utf-8 -*-


def main():

    import yaml

    with open("config.yml", 'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    for section in cfg:
        print(cfg[section])


if __name__ == '__main__':
    main()    