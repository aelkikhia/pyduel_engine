from pyduel_engine.model.card import Card
from pyduel_engine.content.engine_states import Effect as Eff
from pyduel_engine.content.engine_states import Modifier as Mod
# from pyduel_engine.content.engine_states import Target


# ############################ Combat Utilities ###############################


def combat(atk, dfs):
    """Standard Combat Card"""
    return Card({'name': '{0}|{1}'.format(str(atk), str(dfs)),
                 'atk': atk, 'dfs': dfs})


def combat_hand_size():
    return Card({'name': '*|*', 'atk': -1, 'dfs': -1,
                 'effects': [_mod_atk(Mod.hand_size),
                             _mod_dfs(Mod.hand_size)]})


def atk_disc_draw(atk, disc=1, num_cards=1):
    """attack and draw card"""
    return Card({'name': 'atk, disc to draw', 'atk': atk,
                 'effects': [_discard(disc), _draw(num_cards)]})


def atk_disc(atk, disc=1):
    return Card({'name': 'atk and disc', 'atk': atk,
                 'effects': [_discard(disc)]})


def atk_drain(atk):
    return Card({'name': 'drain atk', 'atk': atk, 'effects': [_drain()]})


def atk_draw(atk, num_draw=1, dfs=None):
    """attack and draw card"""
    return Card({'name': 'atk_draw', 'atk': atk, 'dfs': dfs,
                 'effects': [_draw(num_draw)]})


def atk_alone(atk):
    """attack, if unblocked deal different damage"""
    return Card({'name': 'atk alone', 'atk': atk,
                 'effects': [_alone()]})


def atk_move(atk, num_moves=None):
    """attack and move card"""
    return Card({'name': 'atk move', 'atk': atk,
                 'effects': [_move(num_moves)]})


def atk_not_act(atk):
    """attack and not action"""
    return Card({'name': 'atk not action', 'atk': atk,
                 'effects': [_mod_act()]})


def atk_unblocked(atk, alt_atk):
    """attack, if unblocked deal different damage"""
    return Card({'name': 'atk unblocked', 'atk': atk,
                 'effects': [_unblocked(alt_atk)]})


def kill_or_die(atk):
    """kill target or die"""
    return Card({'name': 'kill or die', 'atk': atk,
                 'effects': [_kill_or_die()]})


# ######################### DEFENSE CARDS ####################################


def def_dmg(dfs, dmg):
    """defend and deal damage"""
    return Card({'name': 'def dmg', 'dfs': dfs, 'effects': [_dmg(dmg)]})


def def_draw(dfs, num_draw=1):
    """defend and draw"""
    return Card({'name': 'def draw', 'dfs': dfs, 'effects': [_draw(num_draw)]})


# ######################### SPECIAL CARDS ####################################

def discard(num_cards=None, rand=True):
    """discard card"""
    return Card({'name': 'discard', 'effects': [_discard(num_cards, rand)]})


def discard_draw(num_draw, num_disc=None):
    return Card({'name': 'discard draw', 'effects': [_discard(num_disc),
                                                     _draw(num_draw)]})


def dmg_discard(dmg, num_cards):
    return Card({'name': 'dmg discard', 'effects': [_dmg(dmg),
                                                    _discard(num_cards)]})


def dmg_less_act(dmg, act=1):
    return Card({'name': 'dmg less action', 'effects': [_dmg(dmg),
                                                        _mod_act(act)]})


def dmg_splash_dmg(dmg):
    return Card({'effects': [_dmg(dmg), _splash_dmg(dmg)]})


def dmg_shuffle_discard(dmg):
    return Card({'effects': [_dmg(dmg), _shuffle_discard()]})


def draw(num_draw):
    """Draw Cards"""
    return Card({'name': 'draw', 'effects': [_draw(num_draw)]})


def damage(dmg):
    return Card({'name': 'damage', 'effects': [_dmg(dmg)]})


def get_card(card_type, cards):
    return Card({'effects': [_get_card(card_type, cards)]})


def heal(hp):
    return Card({'effects': [_heal(hp), _alone()]})


def heal_no_draw(hp):
    return Card({'effects': [_heal(hp), _cant_draw()]})


def heal_and_move(hp, num_moves):
    return Card({'effects': [_heal(hp), _move(num_moves)]})


def lift():
    return Card({'effects': [_lift()]})


def move_adj_to_not_act():
    return Card({'effects': [_move(), _mod_act()]})


def move_dmg(dmg, num_moves=None):
    return Card({'effects': [_move(num_moves), _dmg(dmg)]})


def move_draw(num_moves, num_draw):
    """Move and draw"""
    return Card({'name': 'move_draw', 'effects': [_move(num_moves),
                                                  _draw(num_draw)]})


def move_draw_squad(num_moves):
    """Squad Move and draw"""
    return Card({'effects': [_move(num_moves)]})


def move_squad(num_moves):
    """Move squad"""
    return Card({'name': 'move_squad', 'effects': [_move(num_moves)]})


def move_no_hand_draw(num_moves):
    """Move and if no hand, draw cards"""
    return Card({'effects': [_move(num_moves), _no_hand()]})


def move_not_act(num_moves=None):
    """teleport and not action"""
    return Card({'effects': [_move(num_moves), _mod_act()]})


def push(dmg, num_moves=None):
    """damage and move"""
    return Card({'name': 'push', 'effects': [_move(num_moves), _dmg(dmg)]})


def reorder_draw(num_cards, num_draw):
    return Card({'effects': [_reorder(num_cards), _draw(num_draw)]})


def swap_main_minor():
    return Card({'effects': [_swap()]})


# ############################### Effects ####################################


def _cant_draw():
    return {'effect_type': Eff.cant_draw}


def _discard(num_cards, rand=True):
    return {'effect_type': Eff.dmg, 'num_cards': num_cards, 'rand': rand}


def _dmg(dmg):
    return {'effect_type': Eff.dmg, 'dmg': dmg}


def _drain():
    return {'effect_type': Eff.drain}


def _draw(num_draw):
    return {'effect_type': Eff.draw, 'num_draw': num_draw}


def _get_card(card_type, cards):
    return {'effect_type': Eff.get_card, 'card_type': card_type,
            'cards': cards}


def _heal(hp):
    return {'effect_type': Eff.heal, 'hp': hp}


def _kill_or_die():
    return {'effect_type': Eff.kill_or_die}


def _lift():
    return {'effect_type': Eff.lift}


def _mod_act(acts=1):
    return {'effect_type': Eff.mod_act, 'action': acts}


def _mod_atk(mod):
    return {'effect_type': Eff.mod_atk, 'atk': mod}


def _mod_dfs(mod):
    return {'effect_type': Eff.mod_dfs, 'dfs': mod}


def _move(num_moves=0, target=None):
    return {'effect_type': Eff.move, 'moves': num_moves, 'target': target}


def _reorder(num_cards):
    return {'effect_type': Eff.reorder, 'depth': num_cards}


def _shuffle_discard():
    return {'effect_type': Eff.shuffle_discard}


def _splash_dmg(dmg):
    return {'effect_type': Eff.splash_dmg, 'dmg': dmg}


def _swap():
    return {'effect_type': Eff.swap}


def _unblocked(alt_atk):
    return {'effect_type': Eff.unblocked, 'alt_atk': alt_atk}


# ####################### Effect Conditions ############################


def _alone():
    return {'condition': Eff.alone}


def _no_hand():
    return {'condition': Eff.no_hand_draw}


if __name__ == '__main__':
    card = combat_hand_size()
    print(card)
