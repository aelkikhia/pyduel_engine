
from enum import Enum


class SqState(Enum):
    """Base Square States"""
    light, dark, empty, obstacle, hole = range(0, 5)


class phase(Enum):
    move, action = range(0, 2)


class BoardType(Enum):
    """generic boards"""
    ruins, factory, platform, dais = range(0, 4)


class Roll(Enum):
    """Dice Sides"""
    two_all, three, three_all, four, four_all, five = range(0, 6)


class Action(Enum):
    """Squad Actions
    draw card, use card, heal character, skip move"""
    draw, play, heal, skip = range(0, 4)


class Focus(Enum):
    all_chars = 0
    valid_target = 1
    valid_targets = 2
    adj_char = 3
    adj_chars = 2
    minor = 3
    main = 4
    squad = 5


class Target(Enum):
    char = 0
    squad = 1
    minor = 5
    main = 6
    enemy = 7
    # Char Modifiers
    side = 0
    pos = 1
    hp = 2
    is_range = 3
    is_down = 4

    # Squad Modifiers
    deck = 7
    hand = 8
    discard = 9
    actions = 10
    can_draw = 11


class Modifier(Enum):
    hand_size = 0


class CharType(Enum):
    """Base Square States"""
    main, minor = 0, 1


class Effect(Enum):
    alone = 0
    cant_draw = 1
    discard = 2
    dmg = 3
    drain = 4
    draw = 5
    get_card = 6
    heal = 7
    kill_or_die = 8
    less_act = 9
    lift = 10
    mod_atk = 11
    mod_dfs = 12
    move = 13
    no_hand = 14
    mod_act = 15
    reorder = 16
    shuffle_discard = 17
    splash_dmg = 18
    swap = 19
    unblocked = 20


class CardType(Enum):
    """Card Type"""
    combat = 21
    special = 22
    special_combat = 23
    special_atk = 24
    special_def = 25

