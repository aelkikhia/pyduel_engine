
from pyduel_engine.content import default_cards as cd
# from pyduel_engine.content.engine_states import Focus, Target


# ############################### RED DECKS ##################################


# Anakin Skywalker Deck
def anakin_deck():
    return [cd.atk_disc(8), cd.atk_disc(8), cd.move_draw(8, 5),
            cd.move_draw(8, 5), cd.def_dmg(20, 1), cd.def_dmg(20, 1),
            cd.move_dmg(7), cd.move_dmg(7), cd.move_dmg(7)].append(red_deck())


# Luke Skywalker Deck
def luke_deck():
    return [cd.move_draw(6, 2), cd.move_draw(6, 2), cd.move_draw(6, 2),
            cd.discard(), cd.discard(), cd.discard(), cd.atk_alone(4),
            cd.atk_alone(4)].append(red_deck())


# Darth Maul Deck
def maul_deck():
    return [cd.atk_move(8, 6), cd.atk_move(8, 6), cd.atk_move(8, 6),
            cd.atk_not_act(3), cd.atk_not_act(3), cd.atk_not_act(3),
            cd.atk_not_act(4), cd.atk_not_act(4), cd.atk_not_act(4),
            cd.def_dmg(0, 3), cd.def_dmg(0, 3), cd.def_draw(10)
            ].append(red_deck())


# Darth Vader Deck
def vader_deck():
    return [cd.atk_unblocked(3, 20), cd.damage(6), cd.damage(6), cd.damage(6),
            cd.atk_drain(3), cd.atk_drain(3), cd.damage(4), cd.damage(4),
            cd.damage(2), cd.damage(2), cd.damage(2), cd.discard()
            ].append(red_deck())


# ############################### BLUE DECKS ##################################


# Mace Windu Deck
def mace_deck():
    return [cd.atk_draw(5)] * 2 + \
           [cd.combat_hand_size()] * 4 + \
           [cd.damage(4)] * 2 + \
           [cd.move_draw(5, 1)] * 4 + \
        blue_deck()


# Obi-wan Kenobi Deck
def obi_wan_deck():
    return [cd.discard_draw(3), cd.discard_draw(3), cd.discard_draw(3),
            cd.move_draw(8, 1), cd.move_draw(8, 1), cd.atk_move(6, 6),
            cd.atk_move(6, 6), cd.atk_move(6, 6), cd.def_draw(12),
            cd.def_draw(12), cd.def_draw(12), cd.get_card('type', 'discard')
            ].append(blue_deck())


# Count Dooku Deck
def dooku_deck():
    return [cd.atk_draw(7)] * 4 + \
           [cd.push(1)] * 2 + \
           [cd.draw(3)] * 2 + \
           [cd.move_squad(4)] * 3 + \
           [cd.discard(2)] + \
        blue_deck()


# ############################### GREEN DECKS ################################


# Yoda Deck
def yoda_deck():
    return [cd.lift(), cd.lift(), cd.lift(), cd.push(3), cd.push(3),
            cd.def_dmg(20, 0), cd.atk_draw(6), cd.atk_draw(6), cd.discard(1),
            cd.discard(1), cd.def_draw(15), cd.def_draw(15)
            ].append(green_deck())


# Emperor Deck
def emperor_deck():
    return [cd.dmg_discard(3, 1), cd.dmg_discard(3, 1), cd.dmg_discard(3, 1),
            cd.dmg_discard(3, 1), cd.reorder_draw(4, 1), cd.reorder_draw(4, 1),
            cd.discard(2), cd.discard(2), cd.heal_no_draw(4),
            cd.heal_no_draw(4), cd.swap_main_minor(), cd.discard()
            ].append(green_deck())


# ############################## Yellow(+) DECKS #############################


# Han Solo Deck
def han_deck():
    return [cd.atk_disc(4, 1), cd.atk_disc(4, 1), cd.atk_disc(4, 1),
            cd.atk_move(5, 5), cd.atk_move(5, 5), cd.dmg_shuffle_discard(2),
            cd.dmg_shuffle_discard(2)].append(yellow_deck_plus())


# Boba Fett Deck
def boba_deck():
    return [cd.atk_draw(7, 2), cd.atk_draw(9, 3), cd.atk_move(4),
            cd.atk_move(4), cd.atk_move(4), cd.dmg_splash_dmg(4),
            cd.dmg_splash_dmg(4), cd.dmg_less_act(2), cd.dmg_less_act(2)
            ].append(yellow_deck())


# Jango Fett Deck
def jango_deck():
    return [cd.push(2), cd.atk_draw(7, 4), cd.atk_move(4), cd.atk_move(4),
            cd.atk_move(4), cd.move_not_act, cd.dmg_less_act(2),
            cd.dmg_less_act(2)].append(yellow_deck())


# ############################### STRONG DECKS ##############################


# Padme Amidala Deck
def padme_deck():
    return [cd.atk_disc_draw, cd.heal(4), cd.atk_move(6, 6)
            ].append(strong_deck())


# Leia Skywalker Deck
def leia_deck():
    return [cd.atk_draw(7, 1, 7), cd.atk_draw(7, 1, 7), cd.heal(3),
            cd.heal(3)].append(strong_deck())


# Zam Wesell Deck
def zam_deck():
    return [cd.atk_move(7), cd.atk_unblocked(3, 6), cd.atk_unblocked(3, 6),
            cd.atk_unblocked(3, 6)].append(strong_deck())


# Greedo Deck
def greedo_deck():
    return [cd.kill_or_die(7), cd.move_adj_to_not_act(), cd.move_adj_to_not_act
            ].append(strong_deck())


# ############################### STRONG+ DECKS ##############################


# Chewbacca Deck Deck
def chewbacca_deck():
    return [cd.atk_draw(11), cd.push(3, 3), cd.push(3, 3),
            cd.heal_and_move(3, 5), cd.get_card('wookie instinct', 'deck')
            ].append(strong_deck_plus())


# ############################### GENERIC DECKS ##############################


def blue_deck():
    return [cd.combat(5, 1)] * 2 + \
           [cd.combat(4, 2)] * 2 + \
           [cd.combat(3, 3)] * 2 + \
           [cd.combat(1, 4)] * 2 + \
           [cd.combat(4, 1), cd.combat(2, 3)]


def red_deck():
    return [cd.combat(5, 1)] * 4 + \
           [cd.combat(4, 2)] * 2 + \
           [cd.combat(4, 1), cd.combat(3, 2), cd.combat(2, 2), cd.combat(1, 4)]


def green_deck():
    return [cd.combat(4, 2)] * 4 + \
           [cd.combat(3, 3)] * 3 + \
           [cd.combat(2, 4)] * 2 + \
           [cd.combat(1, 5)]


def yellow_deck():
    return [cd.combat(4, 1)] * 3 + \
           [cd.combat(3, 1)] * 2 + \
           [cd.combat(3, 2)] + \
           [cd.combat(2, 2)] * 2 + \
           [cd.combat(1, 4)] * 2


def yellow_deck_plus():
    return [cd.combat(4, 1)] * 3 + \
           [cd.combat(3, 1)] * 2 + \
           [cd.combat(3, 2), cd.combat(2, 2), cd.combat(2, 3)] + \
           [cd.combat(1, 4)] * 2


def weak_deck():
    return [cd.combat(3, 1)] * 2 + \
           [cd.combat(2, 1)] * 3 + \
           [cd.combat(1, 2)] * 4


def strong_deck():
    return [cd.combat(4, 1)] * 2 + \
           [cd.combat(3, 1)] * 2 + \
           [cd.combat(3, 2)] + \
           [cd.combat(2, 3)] * 2 + \
           [cd.combat(1, 4)] * 2


def strong_deck_plus():
    return [cd.combat(5, 1), cd.combat(4, 1)] + \
           [cd.combat(3, 1)] * 2 + \
           [cd.combat(3, 2)] + \
           [cd.combat(2, 3)] * 2 + \
           [cd.combat(1, 4)] * 2


def strong_deck_plus_plus():
    return [cd.combat(5, 1), cd.combat(4, 1)] + \
           [cd.combat(3, 1)] * 2 + \
           [cd.combat(3, 2)] + \
           [cd.combat(2, 3)] * 2 + \
           [cd.combat(2, 4)] + \
           [cd.combat(1, 4)]

if __name__ == '__main__':
    deck = weak_deck()
    print(deck)
