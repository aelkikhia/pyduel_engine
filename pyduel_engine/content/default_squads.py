
from pyduel_engine.epic_plugin.epic_states import Main
from pyduel_engine.content.engine_states import CharType, BoardType
from pyduel_engine.content import default_decks as decks
from pyduel_engine.model.character import Character
from pyduel_engine.model.position import Position


# Light Main Characters
def anakin():
    return Character({'name': 'Anakin Skywalker', 'max_hp': 18,
                      'type': CharType.main, 'deck': decks.anakin_deck(),
                      'init_pos': {BoardType.factory: Position(9, 5),
                                   BoardType.ruins: Position(8, 1),
                                   BoardType.platform: Position(8, 3),
                                   BoardType.dais: Position(9, 6)}})


def han():
    return Character({'name': 'Han Solo', 'max_hp': 13,
                      'type': CharType.main, 'is_range': True,
                      'deck': decks.han_deck(),
                      'init_pos': {BoardType.factory: Position(8, 2),
                                   BoardType.ruins: Position(9, 0),
                                   BoardType.platform: Position(5, 1),
                                   BoardType.dais: Position(6, 6)}})


def luke():
    return Character({'name': 'Luke Skywalker', 'max_hp': 17,
                      'type': CharType.main, 'deck': decks.luke_deck(),
                      'init_pos': {BoardType.factory: Position(5, 3),
                                   BoardType.ruins: Position(6, 0),
                                   BoardType.platform: Position(9, 4),
                                   BoardType.dais: Position(7, 3)}})


def mace():
    return Character({'name': 'Mace Windu', 'max_hp': 19,
                      'type': CharType.main, 'deck': decks.mace_deck(),
                      'init_pos': {BoardType.factory: Position(7, 4),
                                   BoardType.ruins: Position(6, 2),
                                   BoardType.platform: Position(6, 5),
                                   BoardType.dais: Position(8, 2)}})


def obi_wan():
    return Character({'name': 'Obi-Wan Kenobi', 'max_hp': 18,
                      'type': CharType.main, 'deck': decks.obi_wan_deck(),
                      'init_pos': {BoardType.factory: Position(7, 1),
                                   BoardType.ruins: Position(8, 3),
                                   BoardType.platform: Position(5, 3),
                                   BoardType.dais: Position(6, 1)}})


def yoda():
    return Character({'name': 'Yoda', 'max_hp': 15, 'type': CharType.main,
                      'deck': decks.yoda_deck(),
                      'init_pos': {BoardType.factory: Position(9, 1),
                                   BoardType.ruins: Position(9, 4),
                                   BoardType.platform: Position(6, 3),
                                   BoardType.dais: Position(9, 0)}})


# Dark Main Characters
def boba():
    return Character({'name': 'Boba Fett', 'max_hp': 14, 'type': CharType.main,
                      'is_range': True, 'deck': decks.boba_deck(),
                      'init_pos': {BoardType.factory: Position(4, 6),
                                   BoardType.ruins: Position(1, 4),
                                   BoardType.platform: Position(0, 2),
                                   BoardType.dais: Position(2, 5)}})


def dooku():
    return Character({'name': 'Count Dooku', 'max_hp': 18,
                      'type': CharType.main, 'deck': decks.dooku_deck(),
                      'init_pos': {BoardType.factory: Position(1, 3),
                                   BoardType.ruins: Position(4, 6),
                                   BoardType.platform: Position(3, 6),
                                   BoardType.dais: Position(3, 3)}})


def maul():
    return Character({'name': 'Darth Maul', 'max_hp': 18,
                      'type': CharType.main, 'deck': decks.maul_deck(),
                      'init_pos': {BoardType.factory: Position(0, 3),
                                   BoardType.ruins: Position(3, 4),
                                   BoardType.platform: Position(2, 0),
                                   BoardType.dais: Position(2, 3)}})


def vader():
    return Character({'name': 'Darth Vader', 'max_hp': 20,
                      'type': CharType.main, 'deck': decks.vader_deck(),
                      'init_pos': {BoardType.factory: Position(4, 3),
                                   BoardType.ruins: Position(0, 4),
                                   BoardType.platform: Position(1, 6),
                                   BoardType.dais: Position(1, 3)}})


def emperor():
    return Character({'name': 'Emperor Palpatine', 'max_hp': 13,
                      'type': CharType.main, 'deck': decks.emperor_deck(),
                      'init_pos': {BoardType.factory: Position(2, 3),
                                   BoardType.ruins: Position(2, 3),
                                   BoardType.platform: Position(0, 5),
                                   BoardType.dais: Position(0, 3)}})


def jango():
    return Character({'name': 'Jango Fett', 'max_hp': 15,
                      'type': CharType.main, 'is_range': True,
                      'deck': decks.jango_deck(),
                      'init_pos': {BoardType.factory: Position(4, 0),
                                   BoardType.ruins: Position(0, 2),
                                   BoardType.platform: Position(2, 3),
                                   BoardType.dais: Position(2, 2)}})


# Light Minor Characters
def chewbacca():
    return Character({'name': 'Chewbacca', 'max_hp': 15,
                      'type': CharType.minor, 'is_range': True,
                      'deck': decks.chewbacca_deck()})


def leia():
    return Character({'name': 'Leia Skywalker', 'max_hp': 10,
                      'type': CharType.minor, 'is_range': True,
                      'deck': decks.leia_deck()})


def padme():
    return Character({'name': 'Padme Amidala', 'max_hp': 10,
                      'type': CharType.minor, 'is_range': True,
                      'deck': decks.padme_deck()})


def trooper():
    return Character({'name': 'Trooper', 'max_hp': 4, 'type': CharType.minor,
                      'is_range': True, 'deck': decks.weak_deck()})


# Dark Minor Characters
def greedo():
    return Character({'name': 'Greedo', 'max_hp': 7, 'type': CharType.minor,
                      'is_range': True, 'deck': decks.greedo_deck()})


def zam():
    return Character({'name': 'Zam Wesell', 'max_hp': 10,
                      'type': CharType.minor, 'is_range': True,
                      'deck': decks.zam_deck()})


def droid():
    return Character({'name': 'Battle Droid', 'max_hp': 3,
                      'type': CharType.minor, 'is_range': True,
                      'deck': decks.weak_deck()})


def guard():
    return Character({'name': 'Crimson Guard', 'max_hp': 5,
                      'type': CharType.minor, 'is_range': True,
                      'deck': decks.strong_deck_plus_plus()})


def super_droid():
    return Character({'name': 'Super Battle Droid', 'max_hp': 5,
                      'type': CharType.minor, 'is_range': True,
                      'deck': decks.strong_deck_plus()})

SQUADS = {
    Main.anakin: [anakin(), padme()],
    Main.han: [han(), chewbacca()],
    Main.luke: [luke(), leia()],
    Main.mace: [mace(), trooper(), trooper()],
    Main.obi: [obi_wan(), trooper(), trooper()],
    Main.yoda: [yoda(), trooper(), trooper()],
    Main.boba: [boba(), greedo()],
    Main.dooku: [dooku(), super_droid(), super_droid()],
    Main.maul: [maul(), droid(), droid()],
    Main.vader: [vader(), trooper(), trooper()],
    Main.emperor: [emperor(), guard(), guard()],
    Main.jango: [jango(), zam()]}


if __name__ == '__main__':
    test = SQUADS[Main.mace]['main']
    print(test.to_json())
