import logging

from random import shuffle

from pyduel_engine.content.engine_states import SqState, BoardType
from pyduel_engine.utilities import squad_utilities as s_utils
from pyduel_engine.utilities import text_utils as utils
from pyduel_engine.model.engine import Engine

from pyduel_engine.epic_plugin.epic_states import Main

class Game(object):

    def __init__(self):
        self.engine = Engine({})

    def create(self):
        """ sets up game"""
        logging.info('Creating engine')

        # choose number of players 1-6 start with even numbers only
        # pick sides

        # pick squads
        # player_squad = {}
        # for x in range(0, num_squads + 1):
        #     player = utils.choose_player(num_players + 1)
        #     squad = utils.choose_players_squads()
        #     if Player(player) in player_squad:
        #         player_squad[Player(player)].append(Main(squad))
        #     else:
        #         player_squad[Player(player)] = [Main(squad)]
        # print(player_squad)

        # pick number of players
        # num_players = utils.choose_number_of_players()
        num_players = 2

        # pick number of squads
        # self.engine = utils.choose_number_of_squads()
        num_squads = 2

        # skipping the setup menus
        squads = [s_utils.setup_squad(1, Main.dooku, SqState.dark),
                  s_utils.setup_squad(2, Main.mace, SqState.light)]

        # pick boards
        # board_type = utils.choose_game_board()
        board_type = BoardType.ruins

        self.engine = Engine({'num_players': num_players,
                              'squads': squads,
                              'num_squads': len(squads),
                              'board_type': board_type})

        # #################### Notes ###########################

    def initialize_game(self):
        """ Places Characters on the board, draws initial hands
        :return:
        """
        logging.info('Initializing Game')

        # place characters initially on the board
        self.engine.initial_placement()
        logging.info('Placed Main Characters on Board')

        # place secondary characters
        for squad in self.engine.squads:
            pos = self.engine.find_minor_placement(squad)
            # print(pos)
            shuffle(pos)
            # hard coding the first two positions for the minor chars
            self.engine.move_char(squad.chars['minor1'], pos[0])
            self.engine.move_char(squad.chars['minor2'], pos[1])

        logging.info('Placed Minor Characters on Board')

        # shuffle all decks
        self.engine.shuffle_all_decks()
        logging.info('Shuffled all decks')

    # draw default hand
        self.engine.deal_cards_to_squads()
        logging.info('Dealt Cards for initial hand to each squad')

    def start(self):
        """ Starts the game.

        :return:
        """
        logging.info('Starting Game')
        # start game loop
        print(self.engine)

        for x in range(0, 1):
            self.round()

        # while not self.engine.is_game_over():
        #     # loop into squad rounds
        #     self.round()

    def stop(self):
        logging.info('Stopping Game')
        pass

    def save(self):
        logging.info('Saving Game')
        pass

    def quit(self):
        logging.info('Quitting Game')
        pass

    def round(self):
        """ Game round
        cycle through each squad turn
        """
        logging.info('Start Round: {0}'.format(
            self.engine.round))
        # give every squad a turn
        for x in range(0, self.engine.num_squads):
            # loop into turns per squad
            self.turn()
            self.engine.turn += 1

        logging.info('End Round: {0} '.format(self.engine.round))

        # reset turn count
        self.engine.turn = 0
        # increment engine round
        self.engine.round += 1

    def turn(self):
        """ Game turn
        The active squad (set by turn number)
            1. Move
            2. act
        """
        logging.info('Start Turn: {0}'.format(self.engine.turn))

        logging.info("Player: {0} Char: {1} Side: {2} ".format(
            self.engine.active_squad().player_num,
            self.engine.active_squad().chars['main'].name,
            self.engine.active_squad().side.name))

        # Move Phase
        # self.move_phase()

        # Action Phase
        self.action_phase()

        # Increment engine turn
        logging.info('End Turn: {0}'.format(self.engine.turn))

    def move_phase(self):
        """ Move phase for squads
            - Based on dice roll determine if only one character
               or all characters move
            - present moveable characters along with possible moves
            - choose character(s) to move
        """
        logging.info("Start R:{0} T:{1} Move phase".format(
            self.engine.round, self.engine.turn))
        # do you want to move
        if not utils.choose_to_move():
            logging.info("Squad {0} chose not to move".format(
                self.engine.turn + 1))
            return

        # Dice Roll
        self.engine.dice.roll()

        # TODO: Starting and stopping mechanism for moving to the action phase

        # get active characters in squad
        active_chars = self.engine.active_squad().get_active_chars()
        # logging.debug("active characters {0}".format(active_chars))

        # get squad moves
        squad_moves = self._get_and_print_squad_moves(active_chars)

        if self.engine.dice.is_all():
            while len(active_chars) > 0:
                char_key = self._choose_character(active_chars)
                new_pos = self._choose_move(squad_moves[char_key])
                self.engine.move_char(
                    self.engine.active_squad().chars[char_key], new_pos)

        # logging.info("active characters {0}".format(squad_moves))

        logging.info("End R:{0} T:{1} Move phase".format(
            self.engine.round, self.engine.turn))

    def action_phase(self):
        """ Squad performs actions for squad
            Use card, draw card, discard to heal
        """
        logging.info("Start R:{0} T:{1} Action phase".format(
            self.engine.round, self.engine.turn))
        while self.engine.active_squad().has_action():
            logging.info("Start action")

            active_chars = self.engine.active_squad().get_active_chars()
            # active_characters = ['main', 'minor1', 'minor2']
            main_cards, minor_cards = \
                self.engine.active_squad().list_character_cards()
            # chars = {'main': Name: Count Dooku	HP: 18	Pos: (4,6),
            #  'minor1': Name: Super Battle Droid 1	HP: 5	Pos: (4,5),
            #  'minor2': Name: Super Battle Droid 2	HP: 5	Pos: (5,6)}
            print(self.engine.active_squad().hand)
            # hand = [1|4, 1|4, 2|3, 2|3]
            print(main_cards)
            print(minor_cards)
            print(self.engine.get_squad_targets())
            # TODO: check card arrays if conditions allow for use
            # attack, defend, special keys process based on that
            # attack: int, defend: int, special: effects

            possible_actions = self.engine.get_possible_actions()
            action = utils.choose_actions(possible_actions)
            # possible_targets = self.engine.get_possible_targets()
            # targets = self._text_choose_targets(possible_actions)
            # action = Action.draw
            # self.engine.active_action = action
            # if action == Action.card:
                #
                # if card.ack:
                #     get ack from target player
                # self.engine.execute_action()
            #     pass
            # else:
                # self.engine.current_action()
                # self.engine.execute_action()
                # pass

            # def squad_act(self):
            #     if self.active_squad().can_act():
            #         self.active_squad().get_possible_actions()
            #         self.active_squad().actions -= 1
            #
            # def end_turn(self):
            #     pass
            #
            # def end_action(self):
            #     self.target_char = None
            #     self.target_chars = None
            #     self.target_squad = None
            #     self.active_squad().actions -= 1


            logging.info("End Action")
            self.engine.active_squad(). actions -= 1

    def _choose_character(self, active_chars):
        """
        choose char and return char key
        """
        choice = utils.choose_character(active_chars)
        return active_chars.pop(choice)

    def _choose_move(self, positions):
        """
        """
        choice = utils.choose_move(positions)
        return positions[choice]

    def _get_and_print_squad_moves(self, active_chars):
        """ text based helper method
        the squad moves need to be refreshed with every character move
        to allow for newly vacated/occupied squares
        """
        # get squad moves
        squad_moves = self.engine.find_squad_moves(active_chars)

        # print moves
        for k, v in squad_moves.items():
            print(k, ':', v)

        return squad_moves

    def _choose_action(self, chars):
        """
        """
        char = chars.pop(utils.choose_character(chars))
        actions = self.engine.get_possible_moves()
        action = actions[utils.choose_move(actions)]
        return actions


if __name__ == '__main__':
    logging.basicConfig(filename='../pyduel.log', level=logging.INFO)
    # console = logging.StreamHandler()
    # console.setLevel(logging.DEBUG)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # console.setFormatter(formatter)
    # add the handler to the root logger
    # logging.getLogger('').addHandler(console)

    logger = logging.getLogger(__name__)

    new_game = Game()
    new_game.create()
    new_game.initialize_game()
    new_game.start()
    # print(new_game.engine.squads[1].print_deck(verbose=True))
    # print(new_game.engine.squads[0].print_deck(verbose=True))
