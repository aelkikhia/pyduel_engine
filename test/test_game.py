
import unittest

from pyduel_engine.content.engine_states import SqState, Roll
from pyduel_engine.utilities import squad_utilities as s_utils
from pyduel_engine.model.board import Board
from pyduel_engine.game import Game
from pyduel_engine.model.position import Position as Pos

from pyduel_engine.epic_plugin.epic_states import Main
from pyduel_engine.content.engine_states import BoardType

def suite():
    test_suites = unittest.TestSuite()
    test_suites.addTest(TestEngine())
    return test_suites

class TestEngine(unittest.TestCase):

    def setUp(self):
        # load squads
        squads = [s_utils.setup_squad(1, Main.dooku, SqState.dark),
                  s_utils.setup_squad(2, Main.mace, SqState.light)]
        board_type = BoardType.ruins
        self.game = Game({'squads': squads, 'board_type': board_type})
        self.game.num_players = 2
        self.game.initial_placement()
        self.game.active_char = squads[0].chars[0]

    # ######################### active squad ################################

    def test_active_squad(self):
        self.assertIsNotNone(self.game.active_squad())

    # ############################ is_game_over ##############################

    def test_is_game_over_false(self):
        self.game.squads()[0].chars[0].hp = 1
        self.game.squads()[1].chars[0].hp = 1
        self.assertFalse(self.game.is_game_over())

    def test_is_game_over_true(self):
        self.game.squads()[0].chars[0].hp = 0
        # self.squads[2].chars[0].hp = 0
        self.assertTrue(self.game.is_game_over())
