
import unittest
from pyduel_engine.content.engine_states import Roll
from pyduel_engine.model.dice import Dice

def suite():
    test_suites = unittest.TestSuite()
    test_suites.addTest(TestDice())
    return test_suites

class TestDice(unittest.TestCase):

    def setUp(self):
        self.dice = Dice()

    def test_dice(self):
        self.assertIn(self.dice.print_result(), Roll)

    def test__str__(self):
        self.dice.result = Roll.two_all
        self.assertEqual(self.dice.__str__(), 'two_all')

    def test_is_all(self):
        self.dice.result = Roll.two_all
        self.assertEqual(self.dice.is_all(), True)

    def test_num_two_all(self):
        self.dice.result = Roll.two_all
        self.assertEqual(self.dice.num(), 2)

    def test_num_three(self):
        self.dice.result = Roll.three_all
        self.assertEqual(self.dice.num(), 3)

    def test_num_four(self):
        self.dice.result = Roll.four_all
        self.assertEqual(self.dice.num(), 4)

    def test_num_five(self):
        self.dice.result = Roll.five
        self.assertEqual(self.dice.num(), 5)

if __name__ == '__main__':
    unittest.main()
