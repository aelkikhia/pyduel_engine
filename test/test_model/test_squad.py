
import unittest

from pyduel_engine.content.engine_states import CardType, CharType, SqState
from pyduel_engine.model.card import Card
from pyduel_engine.model.character import Character
from pyduel_engine.model.position import Position
from pyduel_engine.model.squad import Squad

def suite():
    test_suites = unittest.TestSuite()
    test_suites.addTest(TestSquad())
    return test_suites

class TestSquad(unittest.TestCase):

    def setUp(self):
        light_main = {'name': 'Main', 'max_hp': 18, 'side': SqState.light,
                      'type': CharType.main}
        light_minor1 = {'name': 'minor1', 'max_hp': 18, 'side': SqState.light,
                        'type': CharType.minor, 'is_range': True}
        light_minor2 = {'name': 'minor2', 'max_hp': 18, 'side': SqState.light,
                        'type': CharType.minor, 'is_range': True}
        dark_main = {'name': 'Main', 'max_hp': 18, 'side': SqState.dark,
                     'type': CharType.main}
        dark_minor1 = {'name': 'minor1', 'max_hp': 18, 'side': SqState.light,
                       'type': CharType.minor, 'is_range': True}
        dark_minor2 = {'name': 'minor2', 'max_hp': 18, 'side': SqState.light,
                       'type': CharType.minor, 'is_range': True}
        self.squad1 = Squad(
            {'player_num': 1, 'side': SqState.light,
             'chars': [Character(light_main), Character(light_minor1),
                       Character(light_minor2)]})
        self.squad2 = Squad(
            {'player_num': 2, 'side': SqState.dark,
             'chars': [Character(dark_main), Character(dark_minor1),
                       Character(dark_minor2)]})
        self.squad3 = Squad(
            {'player_num': 3, 'side': SqState.light,
             'chars': [Character(light_main), Character(light_minor1)]})
        self.squad4 = Squad(
            {'player_num': 4, 'side': SqState.dark,
             'chars': [Character(dark_main), Character(dark_minor1)]})

        self.squad1.chars[0].pos = Position(0, 0)
        self.squad1.chars[1].pos = Position(6, 6)
        self.squad1.chars[2].pos = Position(0, 0)

        self.squad2.chars[0].pos = Position(6, 6)
        self.squad2.chars[1].pos = Position(0, 0)
        self.squad2.chars[2].pos = Position(6, 6)

        self.minor_combat = Card({'card_type': CardType.combat, 'name': '5|1',
                                  'atk': 5, 'dfs': 1,
                                  'owner': self.squad1.chars[1].type})

        self.main_combat = Card({'card_type': CardType.combat, 'name': '5|1',
                                 'atk': 5, 'dfs': 1,
                                 'owner': self.squad1.chars[0].type})

        self.main_special = Card({'card_type': CardType.special, 'name': '5|1',
                                  'atk': 5, 'dfs': 1,
                                  'owner': self.squad1.chars[0].type})

        self.main_pa = Card({'card_type': CardType.combat, 'name': '5|1',
                             'atk': 5, 'dfs': 1,
                             'owner': self.squad1.chars[0].type})

        self.main_pd = Card({'card_type': CardType.combat, 'name': '5|1',
                             'atk': 5, 'dfs': 1,
                             'owner': self.squad1.chars[0].type})

    # ####################### str and repr(squad) ############################

    # def test_str(self):
    #     self.assertEqual(self.squad1.__str__(), '\nPlayer: 1\tMain: Main\t'
    #                                             'Actions: 2\tHand: []')

    # def test_repr(self):
    #     self.assertEqual(self.squad1.__repr__(), '\nPlayer: 1\tMain: Main\t'
    #                                              'Actions: 2\tHand: []')

    # ####################### can_act(squad) ##############################

    def test_can_act_true(self):
        self.assertTrue(self.squad1.has_action())

    def test_can_act_no_actions_false(self):
        self.squad1.actions = 0
        self.assertFalse(self.squad1.has_action())

    # ######################## can_draw_card(squad) #########################

    def test_can_draw_card_true(self):
        self.assertTrue(self.squad1.can_draw_card())

    def test_can_draw_card_false(self):
        self.squad1.can_draw = False
        self.assertFalse(self.squad1.can_draw_card())

    def test_can_draw_card_false_no_actions(self):
        self.squad1.actions = 0
        self.assertFalse(self.squad1.can_draw_card())

    # ############################ draw_card() ##############################

    def test_draw_card_true(self):
        self.squad1.deck.append({'test_card': None})
        deck_length = len(self.squad1.deck)
        hand_length = len(self.squad1.hand)
        self.squad1.draw_card()
        self.assertEqual(len(self.squad1.deck), deck_length - 1)
        self.assertEqual(len(self.squad1.hand), hand_length + 1)

    # ########################### get_card #############################

    def test_get_card(self):
        for x in range(0, 4):
            self.squad1.hand.append(self.main_combat)
        test_card = self.squad1.hand[3]
        self.squad1.play_card(3)
        self.assertEqual(self.squad1.active_card, test_card)

    # ####################### can_play_card(squad) ########################

    def test_can_play_card_true(self):
        self.squad1.hand.append({'test_card': None})
        self.assertTrue(self.squad1.can_play_card())

    def test_can_play_card_false_no_actions(self):
        self.squad1.actions = 0
        self.assertFalse(self.squad1.can_play_card())

    def test_can_play_card_false_no_cards(self):
        self.assertFalse(self.squad1.can_play_card())

    # ############################ discard_card ##############################

    def test_discard_card_random(self):
        self.squad1.hand.append({'test_card': None})
        discard_length = len(self.squad1.discard)
        hand_length = len(self.squad1.hand)
        self.squad1.discard_card()
        self.assertEqual(len(self.squad1.discard), discard_length + 1)
        self.assertEqual(len(self.squad1.hand), hand_length - 1)

    def test_discard_card_index(self):
        for x in range(0, 4):
            self.squad1.hand.append(self.main_combat)
        chosen_card = self.squad1.hand[2]
        self.squad1.discard_card(2)
        self.assertEqual(self.squad1.discard[0], chosen_card)

    # ############################ discard_cards #############################

    def test_discard_cards_random(self):
        for x in range(0, 4):
            self.squad1.hand.append(self.main_combat)
        discard_length = len(self.squad1.discard)
        hand_length = len(self.squad1.hand)
        self.squad1.discard_cards(num_cards=2)
        self.assertEqual(len(self.squad1.discard), discard_length + 2)
        self.assertEqual(len(self.squad1.hand), hand_length - 2)

    def test_discard_cards_list(self):
        for x in range(0, 4):
            self.squad1.hand.append(self.main_combat)
        chosen_card0 = self.squad1.hand[0]
        chosen_card1 = self.squad1.hand[2]
        self.squad1.discard_cards(card_indices=[0, 2])
        self.assertEqual(self.squad1.discard[0], chosen_card0)
        self.assertEqual(self.squad1.discard[1], chosen_card1)

    # ####################### shuffle_discard_into_deck #######################

    def test_shuffle_discard_into_deck(self):
        for x in range(0, 4):
            self.squad1.hand.append(self.main_combat)
        self.squad1.discard_cards(num_cards=4)
        deck_length = len(self.squad1.deck)
        self.squad1.shuffle_discard_into_deck()
        self.assertEqual(len(self.squad1.deck), deck_length + 4)
        self.assertEqual(len(self.squad1.discard), 0)

    # ####################### can_heal_main(squad) ########################

    def test_can_heal_main_true(self):
        self.squad1.chars[1].hp = 0
        self.squad1.chars[2].hp = 0
        self.squad1.chars[0].hp = 2
        self.squad1.hand.append(self.minor_combat)
        self.assertTrue(self.squad1.can_heal_main())

    def test_can_heal_main_no_moves_false(self):
        self.squad1.actions = 0
        self.assertFalse(self.squad1.can_heal_main())

    def test_can_heal_main_false_minors_alive(self):
        self.squad1.hand.append(self.minor_combat)
        self.assertFalse(self.squad1.can_heal_main())

    def test_can_heal_main_false_no_minor_cards(self):
        self.assertFalse(self.squad1.can_heal_main())

    # ####################### can_heal_minor(squad) ########################

    def test_can_heal_minor_true(self):
        self.squad1.chars[0].hp = 0
        self.squad1.chars[1].hp = 3
        self.squad1.hand.append(self.main_combat)
        self.assertTrue(self.squad1.can_heal_minor())

    def test_can_heal_one_minor_true(self):
        self.squad1.chars[0].hp = 0
        self.squad1.chars[1].hp = 5
        self.squad1.chars[2] = None
        self.squad1.hand.append(self.main_combat)
        self.assertTrue(self.squad1.can_heal_minor())

    def test_can_heal_minor_false_main_alive(self):
        self.squad1.actions = 0
        self.squad1.hand.append(self.main_combat)
        self.assertFalse(self.squad1.can_heal_minor())

    def test_can_heal_minor_false_no_main_cards(self):
        self.squad1.chars[0].hp = 0
        self.assertFalse(self.squad1.can_heal_minor())

    # ####################### is_main_dead(squad) #########################

    def test_is_main_dead_false(self):
        self.assertFalse(self.squad1.is_main_dead())

    def test_is_main_dead_true(self):
        self.squad1.chars[0].hp = 0
        self.assertTrue(self.squad1.is_main_dead())

    # ####################### are_minors_dead(squad) #########################

    def test_are_minors_dead_false_both_alive(self):
        self.assertFalse(self.squad1.are_minors_dead())

    def test_are_minors_dead_false_one_exists_and_alive(self):
        self.squad1.chars[2] = None
        self.assertFalse(self.squad1.are_minors_dead())

    def test_are_minors_dead_test_true_only_one_and_dead(self):
        self.squad1.chars[1].hp = 0
        self.squad1.chars.pop(2)
        self.assertTrue(self.squad1.are_minors_dead())

    def test_are_minors_dead_test_false_one_dead(self):
        self.squad1.chars[1].hp = 4
        self.squad1.chars[2].hp = 0
        self.assertFalse(self.squad1.are_minors_dead())

    def test_are_minors_dead_test_true_both_dead(self):
        self.squad1.chars[1].hp = 0
        self.squad1.chars[2].hp = 0
        self.assertTrue(self.squad1.are_minors_dead())

    # ##################### has_hand(squad) #################################

    def test_has_hand_true(self):
        self.squad1.hand.append(self.main_combat)
        self.assertTrue(self.squad1.has_hand())

    def test_has_hand_false(self):
        self.assertFalse(self.squad1.has_hand())

    # ##################### has_main_card(squad) ############################

    def test_has_main_card_true(self):
        self.squad1.hand.append(self.main_combat)
        self.assertTrue(self.squad1.has_main_card())

    def test_has_main_card_false_minor_card_only(self):
        self.squad1.hand.append(self.minor_combat)
        self.assertFalse(self.squad1.has_main_card())

    def test_has_main_card_false_no_hand(self):
        self.assertFalse(self.squad1.has_main_card())

    # ##################### has_minor_card(squad) ###########################

    def test_has_minor_card_true(self):
        self.squad1.hand.append(self.minor_combat)
        self.assertTrue(self.squad1.has_minor_card())

    def test_has_minor_card_false_main_card_only(self):
        self.squad1.hand.append(self.main_combat)
        self.assertFalse(self.squad1.has_minor_card())

    def test_has_minor_card_false_no_hand(self):
        self.assertFalse(self.squad1.has_minor_card())

    # ########################### set_squad_side #############################

    def test_set_squad_side_light_to_dark(self):
        self.squad1.set_squad_side(SqState.dark)
        self.assertEqual(self.squad1.side, SqState.dark)
        self.assertEqual(self.squad1.chars[0].side, SqState.dark)
        self.assertEqual(self.squad1.chars[1].side, SqState.dark)
        self.assertEqual(self.squad1.chars[2].side, SqState.dark)

    # ####################### list_character_cards #########################

    def test_list_character_cards_no_cards(self):
        self.squad1.deck.extend(self.squad1.chars[0].deck)
        self.squad1.deck.extend(self.squad1.chars[1].deck)
        main, minor = self.squad1.list_character_cards()
        self.assertEqual(len(minor), 0)

    def test_list_active_characters_all(self):
        characters = self.squad1.get_active_chars()
        self.assertEqual(len(characters), 3)

    def test_list_active_characters_2(self):
        self.squad1.chars[0].hp = 0
        characters = self.squad1.get_active_chars()
        self.assertEqual(len(characters), 2)

    def test_list_active_characters_1(self):
        self.squad1.chars[0].hp = 0
        self.squad1.chars[1].hp = 0
        characters = self.squad1.get_active_chars()
        self.assertEqual(len(characters), 1)

    def test_list_active_characters_0(self):
        self.squad1.chars[0].hp = 0
        self.squad1.chars[1].hp = 0
        self.squad1.chars[2].hp = 0
        characters = self.squad1.get_active_chars()
        self.assertEqual(len(characters), 0)

    # ##################### Print Methods ###################################

    # def test_json_format(self):
    #     self.assertDictEqual(self.squad1.json_format(), {
    #         'discard': [], 'Hand': [], 'actions': 2, 'player': 1, 'Deck': [],
    #         'side': self.squad1.side, 'chars': self.squad1.chars})

    def test_print_deck(self):
        for x in range(0, 4):
            self.squad1.deck.append(self.main_combat)
        self.assertEqual(self.squad1.print_deck(), None)

    def test_print_hand(self):
        for x in range(0, 4):
            self.squad1.hand.append(self.main_combat)
        self.assertEqual(self.squad1.print_hand(), None)

    def test_print_discard(self):
        for x in range(0, 4):
            self.squad1.discard.append(self.main_combat)
        self.assertEqual(self.squad1.print_discard(), None)


if __name__ == '__main__':
    unittest.main()
