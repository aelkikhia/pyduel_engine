
import unittest
# from pyduel_engine.test_model import card as utils

def suite():
    test_suites = unittest.TestSuite()
    test_suites.addTest(TestCard())
    return test_suites

class TestCard(unittest.TestCase):

    def setUp(self):
        pass
        # self.blank = utils.Card({'card_type': 0, 'effects': []})
        # self.card1 = utils.Card({'card_type': 1, 'atk': 5, 'dfs': 5,
        #                          'effects': [], 'name': '5|1', 'owner': 'bob',
        #                          'desc': 'combat card'})

    # def test_str_return(self):
    #     self.assertEqual(self.card1.__str__(), '5|1')
    #
    # def test_repr_return(self):
    #     self.assertEqual(self.card1.__repr__(), '5|1')
    #
    # def test_eq_true(self):
    #     self.assertTrue(self.card1.__eq__(self.card1))
    #
    # def test_eq_false(self):
    #     self.assertFalse(self.card1.__eq__(self.blank))
    #
    # def test_has_effects_true(self):
    #     self.blank.effects.append(1)
    #     self.assertTrue(self.blank.has_effects())
    #
    # def test_has_effects_false(self):
    #     self.assertFalse(self.card1.has_effects())
    #
    # def test_is_defense_true(self):
    #     self.assertTrue(self.card1.is_dfs())
    #
    # def test_is_defense_false(self):
    #     self.assertFalse(self.blank.is_dfs())
    #
    # def test_is_attack_true(self):
    #     self.assertTrue(self.card1.is_atk())
    #
    # def test_is_attack_false(self):
    #     self.assertFalse(self.blank.is_atk())
    #
    # def test_is_special_true(self):
    #     self.blank.effects.append(1)
    #     self.assertTrue(self.blank.is_special())
    #
    # def test_is_special_false(self):
    #     self.assertFalse(self.card1.is_special())
    #
    # def test_json_format(self):
    #     self.json = self.card1.json_format()
    #     self.assertEqual(self.json['card_type'], 1)
    #
    # def test_print_card(self):
    #     self.assertEqual(self.card1.print_card(), '5|1')
    #
    # def test_print_card_verbose(self):
    #     self.assertEqual(
    #         self.card1.print_card(True),
    #         '5|1 ATK: 5 DEF: 5 DESC: combat card Owner: bob TYPE: 1')
