
import unittest

from pyduel_engine.content.engine_states import CharType
from pyduel_engine.content.engine_states import SqState
from pyduel_engine.model.character import Character
from pyduel_engine.model.position import Position


def suite():
    suites = unittest.TestSuite()
    suites.addTest(TestCharacter())
    return suites


class TestCharacter(unittest.TestCase):

    def setUp(self):
        self.char1 = Character({'name': 'main', 'max_hp': 18, 'side': 'light',
                               'type': 0, 'pos': Position(0, 0)})
        self.char2 = Character({'name': 'main', 'max_hp': 18, 'side': 'dark',
                                'type': 0, 'pos': Position(0, 1)})
        self.new_pos = Position(6, 6)
        self.adj_pos = Position(0, 1)
        self.diagonal_pos = Position(5, 5)
        self.parallel_pos = Position(0, 6)

    def test_str_return(self):
        self.assertEqual(self.char1.__str__(),
                         'Name: main\tHP: 18\tIs Range: False\tSide: light\t'
                         'Pos: (0,0)')

    # def test_repr_return(self):
    #     self.assertEqual(self.char1.__repr__(), 'main')

    def test_switch_pos(self):
        self.char1.switch_pos(self.char2)
        self.assertEqual(self.char1.pos, Position(0, 1))

    def test_kill(self):
        self.char1.kill()
        self.assertEqual(self.char1._hp, 0)
        self.assertEqual(self.char1.pos, None)

    def test_damage_max(self):
        self.char1.damage(50)
        self.assertEqual(self.char1._hp, 0)
        self.assertEqual(self.char1.pos, None)

    def test_damage_minor_damage(self):
        self.char1.damage(6)
        self.assertEqual(self.char1._hp, 12)

    def test_heal_max(self):
        self.char1._hp = 15
        self.char1.heal(10)
        self.assertEqual(self.char1._hp, 18)

    def test_damage_minor_heal(self):
        self.char1._hp = 1
        self.char1.heal(10)
        self.assertEqual(self.char1._hp, 11)

    def test_is_character_on_board_true(self):
        self.assertIsNotNone(self.char1.pos)

    def test_is_character_on_board_false(self):
        self.char1.pos = None
        self.assertIsNone(self.char1.pos)

    def test_able_to_heal_true(self):
        self.char1._hp -= 2
        self.assertTrue(self.char1.is_damaged())

    def test_able_to_heal_false(self):
        self.assertFalse(self.char1.is_damaged())

    def test_is_adj_true(self):
        self.assertTrue(self.char1.is_adj(self.char2))

    def test_is_diagonal_true(self):
        self.char2.pos = self.diagonal_pos
        self.assertTrue(self.char1.is_diagonal(self.char2))

    def test_is_parallel_true(self):
        self.char2.pos = self.parallel_pos
        self.assertTrue(self.char1.is_parallel(self.char2))

    def test_can_target_true(self):
        self.assertTrue(self.char1.is_enemy(self.char2))

    def test_can_target_false(self):
        self.assertFalse(self.char1.is_enemy(self.char1))

    def test_json_format(self):
        self.assertDictEqual(self.char1.to_json(),
                             {'hp': 18, 'is_range': False, 'max_hp': 18,
                              'name': 'main', 'side': 'light',
                              'pos': Position(0, 0)})


if __name__ == '__main__':
    unittest.main()
