
import unittest

from pyduel_engine.content.engine_states import SqState, Roll
from pyduel_engine.utilities import squad_utilities as s_utils
from pyduel_engine.model.board import Board
from pyduel_engine.model.engine import Engine
from pyduel_engine.model.position import Position as Pos

from pyduel_engine.epic_plugin.epic_states import Main

def suite():
    test_suites = unittest.TestSuite()
    test_suites.addTest(TestEngine())
    return test_suites

class TestEngine(unittest.TestCase):

    def setUp(self):
        self.board = Board({})
        self.squads = [s_utils.setup_squad(1, Main.mace, SqState.light),
                       s_utils.setup_squad(2, Main.dooku, SqState.dark)]
        self.engine = Engine({'board': self.board, 'squads': self.squads,
                              'active_char': self.squads[0].chars[0]})
        self.engine.move_char(self.squads[0].chars[0], Pos(3, 3))
        self.test_char = self.squads[0].chars[0]

    # ######################### move_character ################################

    def test_move_character_true_place_character(self):
        self.assertEqual(self.engine.board.board[8][5]['state'], SqState.empty)
        self.engine.move_char(self.test_char, Pos(8, 5))
        self.assertEqual(self.engine.board.board[8][5]['state'], SqState.light)

    def test_move_character_true_move_character(self):
        self.engine.move_char(self.test_char, Pos(8, 5))
        self.assertEqual(self.engine.board.board[8][5]['state'], SqState.light)
        self.assertEqual(self.engine.board.board[2][2]['state'], SqState.empty)
        self.engine.move_char(self.test_char, Pos(2, 2))
        self.assertEqual(self.engine.board.board[2][2]['state'], SqState.light)

    def test_move_character_false_out_of_bounds(self):
        self.assertFalse(self.engine.move_char(self.squads[0].chars[0],
                                               Pos(8, 5)))

    def test_deal_card_to_squads(self):
        deck_length0 = len(self.squads[0].deck)
        hand_length0 = len(self.squads[0].hand)
        deck_length1 = len(self.squads[1].deck)
        hand_length1 = len(self.squads[1].hand)
        self.engine.deal_cards_to_squads(4)
        self.assertEqual(len(self.squads[0].hand), hand_length0 + 4)
        self.assertEqual(len(self.squads[0].deck), deck_length0 - 4)
        self.assertEqual(len(self.squads[1].hand), hand_length1 + 4)
        self.assertEqual(len(self.squads[1].deck), deck_length1 - 4)

    # ####################### get_all_adjacent_characters #####################

    # def test_get_all_adjacent_characters_empty_list(self):
    #     self.squads[0].chars[0].pos = Pos(0, 0)
    #     self.assertEqual(len(self.engine.get_adj_chars(self.test_char)), 0)

    # def test_get_all_adjacent_characters_one_ally_three_enemy(self):
    #     self.engine.remove_char(self.squads[0].chars[1])
    #     self.engine.move_char(self.squads[0].chars[2], Pos(3, 2))
    #     self.engine.move_char(self.squads[1].chars[0], Pos(4, 3))
    #     self.engine.move_char(self.squads[1].chars[1], Pos(4, 2))
    #     self.engine.move_char(self.squads[1].chars[2], Pos(4, 4))
    #     self.assertEqual(len(self.engine.get_adj_chars(self.test_char)), 4)
    #
    # def test_get_all_adjacent_characters_one_enemy(self):
    #     self.engine.remove_char(self.squads[0].chars[1])
    #     self.engine.remove_char(self.squads[0].chars[2])
    #     self.engine.move_char(self.squads[1].chars[0], Pos(4, 3))
    #     self.engine.remove_char(self.squads[1].chars[1])
    #     self.engine.remove_char(self.squads[1].chars[2])
    #     self.assertEqual(len(self.engine.get_adj_chars(self.test_char)), 1)

    # ######################## get_all_adjacent_allies ####################

    # def test_get_all_adjacent_allies_empty_list(self):
    #     self.squads[0].chars[0].pos = Pos(0, 0)
    #     allies, enemies = self.engine.get_adj_allies_enemies(
    #         self.squads[0].chars[0])
    #     self.assertListEqual(allies, [])
    #
    # def test_get_all_adjacent_allies_two(self):
    #     self.engine.move_char(self.squads[0].chars[1], Pos(3, 4))
    #     self.engine.move_char(self.squads[0].chars[2], Pos(3, 2))
    #     allies, enemies = self.engine.get_adj_allies_enemies(
    #         self.squads[0].chars[0])
    #     self.assertEqual(len(allies), 2)
    #
    # def test_get_all_adjacent_allies_one(self):
    #     self.engine.move_char(self.squads[0].chars[2], Pos(3, 2))
    #     allies, enemies = self.engine.get_adj_allies_enemies(
    #         self.squads[0].chars[0])
    #     self.assertEqual(len(allies), 1)
    #
    # # ######################## get_all_adjacent_enemies ####################
    #
    # def test_get_all_adjacent_enemies_empty_list(self):
    #     self.engine.remove_char(self.squads[1].chars[0])
    #     allies, enemies = self.engine.get_adj_allies_enemies(
    #         self.squads[0].chars[0])
    #     self.assertListEqual(enemies, [])
    #
    # def test_get_all_adjacent_enemies_three(self):
    #     self.engine.move_char(self.squads[1].chars[0], Pos(4, 3))
    #     self.engine.move_char(self.squads[1].chars[1], Pos(4, 2))
    #     self.engine.move_char(self.squads[1].chars[2], Pos(4, 4))
    #     self.squads[1].chars[0].hp = 3
    #     self.squads[1].chars[1].hp = 2
    #     self.squads[1].chars[2].hp = 1
    #     allies, enemies = self.engine.get_adj_allies_enemies()
    #     self.assertEqual(len(enemies), 3)
    #
    # def test_get_all_adjacent_enemies_two(self):
    #     self.engine.remove_char(self.squads[1].chars[0])
    #     self.engine.move_char(self.squads[1].chars[1], Pos(4, 2))
    #     self.engine.move_char(self.squads[1].chars[2], Pos(4, 4))
    #     allies, enemies = self.engine.get_adj_allies_enemies()
    #     self.assertEqual(len(enemies), 2)

    #########################################################################
    #                       Discovery Methods
    #########################################################################

    # ######################## get_possible_targets ####################

    # def test_get_possible_targets_empty_list(self):
    #     self.engine.move_char(self.squads[0].chars[2], Pos(3, 2))
    #     self.engine.move_char(self.squads[1].chars[0], Pos(6, 6))
    #     self.engine.move_char(self.squads[1].chars[1], Pos(0, 3))
    #     self.engine.move_char(self.squads[1].chars[2], Pos(4, 4))
    #     self.assertEqual(len(self.engine.get_possible_targets(
    #         self.squads[0].chars[0])), 1)
    #
    # def test_get_possible_targets_three(self):
    #     self.engine.move_char(self.squads[0].chars[0], Pos(2, 2))
    #     self.engine.move_char(self.squads[0].chars[2], Pos(3, 3))
    #     self.engine.move_char(self.squads[1].chars[0], Pos(6, 6))
    #     self.engine.move_char(self.squads[1].chars[1], Pos(0, 3))
    #     self.engine.move_char(self.squads[1].chars[2], Pos(4, 4))
    #     self.squads[1].chars[0].hp = 17
    #     self.assertEqual(len(self.engine.get_possible_targets(
    #         self.squads[0].chars[2])), 3)
    #
    # def test_get_possible_targets_two(self):
    #     self.engine.remove_char(self.squads[1].chars[0])
    #     self.engine.move_char(self.squads[1].chars[1], Pos(4, 2))
    #     self.engine.move_char(self.squads[1].chars[2], Pos(4, 4))
    #     self.assertEqual(len(self.engine.get_possible_targets(
    #         self.squads[0].chars[0])), 2)

    # ########################### Get Possible Moves ########################

    def test_find_moves_roll_1_true(self):
        self.assertEqual(len(self.engine.get_possible_moves(self.test_char,
                                                            num_moves=1)), 4)

    def test_find_moves_roll_2_true(self):
        self.engine.dice.result = Roll.two_all
        self.assertEqual(len(self.engine.get_possible_moves(
            self.test_char)), 13)

    def test_find_moves_roll_3_true(self):
        self.engine.dice.result = Roll.three
        self.assertEqual(len(self.engine.get_possible_moves(
            self.test_char)), 25)

    def test_find_moves_roll_4_true(self):
        self.engine.dice.result = Roll.four
        self.assertEqual(len(self.engine.get_possible_moves(
            self.test_char)), 38)

    def test_find_moves_roll_5_true(self):
        self.engine.dice.result = Roll.five
        self.assertEqual(len(self.engine.get_possible_moves(
            self.test_char)), 49)

    ##########################################################################
    #                           Abilities
    ##########################################################################

    # ############################ Damage_Adjacent ###########################

    # def test_dmg_adjacent_enemies(self):
    #     self.engine.move_char(self.squads[0].chars[1], Pos(5, 6))
    #     self.engine.move_char(self.squads[0].chars[2], Pos(3, 2))
    #     self.engine.move_char(self.squads[1].chars[0], Pos(4, 3))
    #     self.engine.move_char(self.squads[1].chars[1], Pos(4, 2))
    #     self.engine.move_char(self.squads[1].chars[2], Pos(4, 4))
    #     self.engine.damage_adj_chars(2)
    #     self.assertEqual(self.squads[0].chars[1].hp, 4)
    #     self.assertEqual(self.squads[1].chars[0].hp, 16)
    #     self.assertEqual(self.squads[1].chars[2].hp, 3)

    ###########################################################################
    #                        Card mechanics
    ###########################################################################

    # def test_attack_defended(self):
    #     self.engine.def_card = 'fake card'
    #     self.assertIsNotNone(self.engine.def_card)


if __name__ == '__main__':
    unittest.main()
