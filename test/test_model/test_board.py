
import unittest

from pyduel_engine.content.engine_states import BoardType, CharType, SqState
from pyduel_engine.model.board import Board
from pyduel_engine.model.character import Character
from pyduel_engine.model.position import Position as Pos

def suite():
    test_suites = unittest.TestSuite()
    test_suites.addTest(TestBoard())
    return test_suites

class TestBoard(unittest.TestCase):

    def setUp(self):
        self.char1 = Character({'max_hp': 18, 'type': CharType.main,
                                'side': SqState.light, 'pos': Pos(0, 0)})
        self.char2 = Character({'max_hp': 19, 'type': CharType.main,
                                'side': SqState.dark, 'pos': Pos(6, 6)})
        self.pos = Pos(5, 6)
        self.b1 = Board({})
        self.b2 = Board({'name': 'ruins', 'board_type': BoardType.dais})

    # ############################# __str__ #################################

    def test_string_method(self):
        self.b2.set_sqr_state(Pos(0, 0), SqState.light)
        self.b2.set_sqr_state(Pos(0, 1), SqState.dark)
        self.assertEqual(self.b2.__str__(), 'BoardType.dais\n'
                                            '    0123456789\n'
                                            '--------------\n'
                                            '0 | LOOOOOEEEE\n'
                                            '1 | DOEEEEEHEH\n'
                                            '2 | EEEEEEHHEH\n'
                                            '3 | EEEEEEEEEE\n'
                                            '4 | EEEEEEHHEH\n'
                                            '5 | OOEEEEEHEH\n'
                                            '6 | OOOOOOEEEE\n')

    # ######################## square_state ##################################

    def test_square_state_board_empty_true(self):
        self.assertEqual(self.b1.get_sqr_state(self.pos), SqState.empty)

    def test_square_state_board_empty_false(self):
        self.assertNotEqual(self.b1.get_sqr_state(self.pos), SqState.light)

    # ######################## is_diagonal ####################################

    def test_if_square_is_diagonal_true(self):
        self.assertTrue((self.char1.pos, self.char2.pos))

    def test_if_square_is_diagonal_false(self):
        self.char2.pos = self.pos
        self.assertFalse(self.char1.pos.is_diagonal(self.char2.pos))

    # ######################## is_parallel ####################################

    def test_if_squares_are_parallel_false(self):
        self.assertFalse(self.char1.pos.is_parallel(self.char2.pos))

    def test_if_squares_are_parallel_true(self):
        self.char2.pos = Pos(0, 6)
        self.assertTrue(self.char1.pos.is_parallel(self.char2.pos))

    # ####################### is_adj ####################################

    def test_if_squares_are_adj_false(self):
        self.assertFalse(self.char1.pos.is_adj(self.char2.pos))

    def test_if_squares_are_adj_true(self):
        self.char2.pos = Pos(0, 1)
        self.assertTrue(self.char1.pos.is_adj(self.char2.pos))

    # ######################## is_parallel_clear ##############################

    def test_if_parallel_squares_clear_false_not_parallel(self):
        self.assertFalse(self.b1.is_parallel_clr(self.char1.pos,
                                                 self.char2.pos))

    def test_parallel_squares_clear_on_x_axis_orig_lt_target_true(self):
        self.assertTrue(self.b1.is_parallel_clr(Pos(2, 2), Pos(2, 5)))

    def test_parallel_squares_clear_on_x_axis_orig_gt_target_true(self):
        self.assertTrue(self.b1.is_parallel_clr(Pos(2, 5), Pos(2, 2)))

    def test_parallel_squares_clear_on_x_axis_target_lt_orig_true(self):
        self.assertTrue(self.b1.is_parallel_clr(Pos(2, 2), Pos(2, 5)))

    def test_parallel_squares_clear_on_x_axis_target_gt_orig_true(self):
        self.assertTrue(self.b1.is_parallel_clr(Pos(2, 2), Pos(5, 2)))

    def test_parallel_squares_clear_on_x_axis_adj_true(self):
        self.assertTrue(self.b1.is_parallel_clr(Pos(2, 2), Pos(2, 3)))

    def test_parallel_squares_clear_on_y_axis_orig_gt_target_true(self):
        self.assertTrue(self.b1.is_parallel_clr(Pos(5, 2), Pos(2, 2)))

    def test_parallel_squares_are_clear_on_x_plane_adj_false(self):
        self.b1.board[0][3]['state'] = SqState.obstacle
        self.assertFalse(self.b1.is_parallel_clr(Pos(0, 0), Pos(0, 6)))

    def test_parallel_squares_are_clear_on_y_plane_adj_false(self):
        self.b1.board[3][0]['state'] = SqState.obstacle
        self.assertFalse(self.b1.is_parallel_clr(Pos(0, 0), Pos(6, 0)))

    # ######################## is_diagonal_clear ##############################

    def test_board_is_diagonal_clear_true(self):
        self.assertTrue(self.b1.is_diagonal_clr(Pos(1, 3), Pos(4, 0)))

    def test_board_is_diagonal_clear_true_inverse(self):
        self.assertTrue(self.b1.is_diagonal_clr(Pos(1, 3), Pos(4, 0)))

    def test_board_is_diagonal_clear_false(self):
        self.b1.board[2][2]['state'] = SqState.dark
        self.assertFalse(self.b1.is_diagonal_clr(Pos(1, 3), Pos(4, 0)))

    # ####################### is_obstructed ##############################

    def test_is_obstructed_surrounded_true(self):
        self.char1.pos = Pos(3, 5)
        self.b1.board[3][6]['state'] = SqState.dark
        self.b1.board[4][5]['state'] = SqState.dark
        self.b1.board[2][5]['state'] = SqState.dark
        self.b1.board[3][4]['state'] = SqState.dark
        self.assertTrue(self.b1.is_obstructed(self.char1))

    def test_is_obstructed_obstacles_true(self):
        self.char1.pos = Pos(3, 5)
        self.b1.board[3][6]['state'] = SqState.obstacle
        self.b1.board[4][5]['state'] = SqState.obstacle
        self.b1.board[2][5]['state'] = SqState.obstacle
        self.b1.board[3][4]['state'] = SqState.obstacle
        self.assertTrue(self.b1.is_obstructed(self.char1))

    def test_is_obstructed_false_one_friendly(self):
        self.char1.pos = Pos(3, 5)
        self.b1.board[3][6]['state'] = SqState.light
        self.b1.board[4][5]['state'] = SqState.dark
        self.b1.board[2][5]['state'] = SqState.dark
        self.b1.board[3][4]['state'] = SqState.dark
        self.assertFalse(self.b1.is_obstructed(self.char1))

    def test_is_obstructed_false_one_empty(self):
        self.char1.pos = Pos(3, 5)
        self.b1.board[3][6]['state'] = SqState.dark
        self.b1.board[4][5]['state'] = SqState.dark
        self.b1.board[2][5]['state'] = SqState.dark
        self.b1.board[3][4]['state'] = SqState.empty
        self.assertFalse(self.b1.is_obstructed(self.char1))

    # ######################## is_out_of_bounds ##############################

    def test_is_out_of_bounds_true_x(self):
        self.assertTrue(self.b1.out_of_bounds(Pos(10, 6)))

    def test_is_out_of_bounds_true_y(self):
        self.assertTrue(self.b1.out_of_bounds(Pos(7, 9)))

    def test_is_out_of_bounds_false(self):
        self.assertFalse(self.b1.out_of_bounds(Pos(9, 6)))

    # ######################## can_move_through ##############################

    def test_can_move_through_true_empty(self):
        self.b1.board[3][5]['state'] = SqState.empty
        self.assertTrue(self.b1.can_move_through(self.char1, Pos(3, 5)))

    def test_can_move_through_true_ally(self):
        self.b1.board[3][5]['state'] = SqState.light
        self.assertTrue(self.b1.can_move_through(SqState.light, Pos(3, 5)))

    def test_can_move_through_false_enemy(self):
        self.b1.board[3][5]['state'] = SqState.dark
        self.assertFalse(self.b1.can_move_through(self.char1, Pos(3, 5)))

    def test_can_move_through_false_hole(self):
        self.b1.board[3][5]['state'] = SqState.hole
        self.assertFalse(self.b1.can_move_through(self.char1, Pos(3, 5)))

    def test_can_move_through_false_obstacle(self):
        self.b1.board[3][5]['state'] = SqState.obstacle
        self.assertFalse(self.b1.can_move_through(self.char1, Pos(3, 5)))

    # ######################## is_valid_move ##############################
    # already covered out_of_bounds and can_move_through methods

    # ######################## can_target ##############################

    def test_can_target_attack_ally_adj(self):
        self.char2.pos = Pos(0, 1)
        self.assertTrue(self.b1.can_target(self.char1, self.char2))

    def test_can_target_attack_false(self):
        self.char1.is_range = False
        self.assertFalse(self.b1.can_target(self.char1, self.char2))

    def test_can_target_attack_true(self):
        self.char1.is_range = True
        self.assertTrue(self.b1.can_target(self.char1, self.char2))

    # ######################## find_moves ##############################

    def test_find_moves_roll_1_true(self):
        self.char1.pos = Pos(3, 4)
        self.assertEqual(len(self.b1.find_moves(self.char1, 1)), 4)

    def test_find_moves_roll_2_true(self):
        self.char1.pos = Pos(3, 4)
        self.assertEqual(len(self.b1.find_moves(self.char1, 2)), 13)

    def test_find_moves_roll_3_true(self):
        self.char1.pos = Pos(3, 4)
        self.assertEqual(len(self.b1.find_moves(self.char1, 3)), 24)

    def test_find_moves_roll_4_true(self):
        self.char1.pos = Pos(3, 4)
        self.assertEqual(len(self.b1.find_moves(self.char1, 4)), 36)

    def test_find_moves_roll_5_true(self):
        self.char1.pos = Pos(3, 4)
        self.assertEqual(len(self.b1.find_moves(self.char1, 5)), 47)

    # ######################## get_adj_empty ##############################

    def test_get_adj_empty_pos_out_of_bounds(self):
        self.assertEqual(self.b1.get_adj_empty_pos(Pos(-1, -1)), [])

    def test_get_adj_empty_pos_none(self):
        self.b1.board[4][3]['state'] = SqState.light
        self.b1.board[4][5]['state'] = SqState.light
        self.b1.board[3][4]['state'] = SqState.light
        self.b1.board[5][4]['state'] = SqState.light
        self.b1.board[3][3]['state'] = SqState.light
        self.b1.board[3][5]['state'] = SqState.light
        self.b1.board[5][3]['state'] = SqState.light
        self.b1.board[5][5]['state'] = SqState.light
        self.assertEqual(self.b1.get_adj_empty_pos(Pos(4, 4)), [])

    def test_get_adj_empty_pos_4(self):
        self.b1.board[3][3]['state'] = SqState.light
        self.b1.board[3][5]['state'] = SqState.light
        self.b1.board[5][3]['state'] = SqState.light
        self.b1.board[5][5]['state'] = SqState.light
        self.assertEqual(self.b1.get_adj_empty_pos(Pos(4, 4)),
                         [Pos(4, 3), Pos(4, 5), Pos(3, 4), Pos(5, 4)])

    # ######################## get_adj_empty ##############################

    def test_print_board_state(self):
        self.b2.set_sqr_state(Pos(0, 0), SqState.light)
        self.b2.set_sqr_state(Pos(0, 1), SqState.dark)
        self.assertEqual(
            self.b2.print_board_coordinates(),
            'BoardType.dais\n'
            '     0    1    2    3    4    5    6    7    8    9   \n'
            '------------------------------------------------------\n'
            '0 | (0,0)(1,0)(2,0)(3,0)(4,0)(5,0)(6,0)(7,0)(8,0)(9,0)\n'
            '1 | (0,1)(1,1)(2,1)(3,1)(4,1)(5,1)(6,1)(7,1)(8,1)(9,1)\n'
            '2 | (0,2)(1,2)(2,2)(3,2)(4,2)(5,2)(6,2)(7,2)(8,2)(9,2)\n'
            '3 | (0,3)(1,3)(2,3)(3,3)(4,3)(5,3)(6,3)(7,3)(8,3)(9,3)\n'
            '4 | (0,4)(1,4)(2,4)(3,4)(4,4)(5,4)(6,4)(7,4)(8,4)(9,4)\n'
            '5 | (0,5)(1,5)(2,5)(3,5)(4,5)(5,5)(6,5)(7,5)(8,5)(9,5)\n'
            '6 | (0,6)(1,6)(2,6)(3,6)(4,6)(5,6)(6,6)(7,6)(8,6)(9,6)\n')

if __name__ == '__main__':
    unittest.main()
