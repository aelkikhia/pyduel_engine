
import unittest

from pyduel_engine.model import position as pos

def suite():
    test_suites = unittest.TestSuite()
    test_suites.addTest(TestPosition())
    return test_suites

class TestPosition(unittest.TestCase):

    def setUp(self):
        self.pos = pos.Position(0, 0)
        self.pos_diagonal = pos.Position(6, 6)
        self.pos_adj = pos.Position(0, 1)
        self.pos_parallel = pos.Position(0, 6)

    # ####################### Positional Methods ##############################

    def test_pos_is_diagonal_true(self):
        self.assertTrue(self.pos.is_diagonal(self.pos_diagonal))

    def test_pos__eq__true(self):
        self.assertEqual(self.pos, self.pos)

    def test_pos__neq__true(self):
        self.assertNotEqual(self.pos, self.pos_diagonal)

    def test__repr__(self):
        self.assertEqual(self.pos.__repr__(), '(0,0)')

    def test_pos_is_diagonal_false(self):
        self.assertFalse(self.pos.is_diagonal(self.pos_parallel))

    def test_pos_is_adj_true(self):
        self.assertTrue(self.pos.is_adj(self.pos_adj))

    def test_pos_is_adj_false(self):
        self.assertFalse(self.pos.is_adj(self.pos_diagonal))

    def test_pos_is_parallel_true(self):
        self.assertTrue(self.pos.is_parallel(self.pos_parallel))

    def test_pos_is_parallel_false(self):
        self.assertFalse(self.pos.is_parallel(self.pos_diagonal))

    def test_pos_json_format(self):
        self.assertDictEqual(self.pos.to_json(), {'x': self.pos.x,
                                                      'y': self.pos.y})

    # ######################### Pos Utilities ###############################

    def test_shift_up(self):
        self.assertEqual(pos.shift_up(self.pos_diagonal), pos.Position(6, 7))

    def test_shift_down(self):
        self.assertEqual(pos.shift_down(self.pos_diagonal), pos.Position(6, 5))

    def test_shift_right(self):
        self.assertEqual(pos.shift_right(self.pos_diagonal),
                         pos.Position(7, 6))

    def test_shift_left(self):
        self.assertEqual(pos.shift_left(self.pos_diagonal), pos.Position(5, 6))

    def test_shift_up_left(self):
        self.assertEqual(pos.shift_up_left(self.pos_diagonal),
                         pos.Position(7, 5))

    def test_shift_down_left(self):
        self.assertEqual(pos.shift_down_left(self.pos_diagonal),
                         pos.Position(5, 5))

    def test_shift_up_right(self):
        self.assertEqual(pos.shift_up_right(self.pos_diagonal),
                         pos.Position(7, 7))

    def test_shift_down_right(self):
        self.assertEqual(pos.shift_down_right(self.pos_diagonal),
                         pos.Position(5, 7))

if __name__ == '__main__':
    unittest.main()
