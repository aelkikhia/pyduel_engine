
from PyQt4.QtGui import QMainWindow
from pyduel_gui.widgets import game_status_widget, setup_widget, game_widget, \
    menu_widget
from pyduel_gui.widgets.game_widget import GameWidget


class GameWindow(QMainWindow):

    def __init__(self, game=None, parent=None):
        super(GameWindow, self).__init__(parent)

        # game engine
        self.game = game
        if game is None:
            self.setup_dialog()
        self.__build_gui()

    ##########################################################################
    # Game Window Build
    ##########################################################################

    def __build_gui(self):
        """Instantiates gui : Menu, status bar and main widget"""

        # Game widget
        self.game_widget = GameWidget(self)
        self.setCentralWidget(self.game_widget)
        # status bar
        self.statusBar().addWidget(game_status_widget.StatusBar(
            "{0}\tInitializing...".format(self.game.print_status())))
        # menu
        self.setMenuBar(menu_widget.GameMenu(self))

    def setup_dialog(self):
        """Setup dialogue for the game"""
        setup_dialog = setup_widget.SetupWidget()
        self.game, ok = setup_dialog.engineSetup()
        if ok:
            return True

    def open_file(self):
        pass

    def start_game(self):
        self.game_widget.start_game()

if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication

    app = QApplication(sys.argv)
    from pyduel_engine.content.engine_states import BoardType
    from pyduel_engine.utilities import squad_utilities as s_utils
    from pyduel_engine.content.engine_states import SqState as SqState
    from pyduel_engine.game import Game
    from pyduel_engine.epic_plugin.epic_states import Main
    squads = [s_utils.setup_squad(1, Main.dooku, SqState.dark),
              s_utils.setup_squad(2, Main.mace, SqState.light)]
    board_type = BoardType.dais
    test_game = Game({'squads': squads, 'board_type': board_type})
    test_game.num_players = 2
    win = GameWindow(test_game)
    win.show()
    sys.exit(app.exec_())
