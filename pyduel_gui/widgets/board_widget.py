#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt4.QtGui import QSizePolicy, QGridLayout, QWidget
from PyQt4.QtCore import SIGNAL, QObject
from pyduel_gui.widgets.square_widget import Square

# TODO: separate engine from gui
from pyduel_engine.content.engine_states import SqState
from pyduel_engine.model.position import Position


class BoardWidget(QWidget):
    """ Game Board Gui """

    def __init__(self, board, parent=None):
        super(BoardWidget, self).__init__(parent)
        self.__buildBoard(board)

    def getSquare(self, pos):
        """Gets the square object at position pos """
        return self.gui_board[pos.x][pos.y]

    def squareSelect(self, pos):
        """Select square at pos """
        self.getSquare(pos).select()

    def squareDeselect(self, pos):
        """Deselect Square at pos """
        self.getSquare(pos).deselect()

    def activateSquares(self, pos_list):
        """activate squares in list of positions [pos1, pos2, n] """
        for pos in pos_list:
            self.activateSquare(pos)

    def activateSquare(self, pos):
        self.getSquare(pos).activate()

    def deactivateSquares(self, pos_list):
        """unlocks and reset squares in position list """
        for pos in pos_list:
            self.deactivateSquare(pos)

    def deactivateSquare(self, pos):
        self.getSquare(pos).deactivate()

    def moveChar(self, origin_pos, target_pos, side, name):
        """places character in new position on the gui """
        self.emptySquare(origin_pos)
        self.getSquare(target_pos).setSquare(side, name)

    def placeChar(self, pos, side, name):
        """places character in new position on the gui """
        self.getSquare(pos).setSquare(side, name)

    def emptySquare(self, pos):
        """Empties square at pos, deselects it and disables it """
        self.getSquare(pos).setEmpty()

    def unlockSquares(self, positions):
        """enables squares in the list of positions [pos1, pos2, n] """
        for pos in positions:
            self.getSquare(pos).setDisabled(False)

    def lockSquares(self, positions):
        """Disables squares in the list of positions [pos1, pos2, n] """
        for pos in positions:
            self.getSquare(pos).setDisabled(True)

    def _squareSelected(self, pos):
        """Sends selected square pos to game widget"""
        self.emit(SIGNAL("select"), pos)

    ##########################################################################
    # Build the Board
    ##########################################################################
    def __buildBoard(self, board, vertical=None):

        # dimensions
        policy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        self.setSizePolicy(policy)

        # layout
        self.layout = QGridLayout(self)
        self.layout.setSpacing(0)

        # gui board dictionary
        self.gui_board = dict()

        # TODO: extract the Position object out of code or make
        # TODO: independent of the duel engine
        for row in range(0, board.max_x):
            # declare the row key points to col dictionary
            self.gui_board[row] = {}
            for col in range(0, board.max_y):

                # position object for convenience
                pos = Position(row, col)

                # square widget
                square = Square(state=board.sqr_state(pos), position=pos)
                # Disable the squares by default
                QObject.connect(square, SIGNAL("select"), self._squareSelected)

                # Add the square to the gui board dictionary
                self.gui_board[row][col] = square

                # Add the square to the gui layouts
                if vertical is None:
                    self.layout.addWidget(square, col, row)
                else:
                    self.layout.addWidget(square, row, col)


if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication

    from pyduel_engine.content.engine_states import BoardType
    from pyduel_engine.utilities import squad_utilities as s_utils
    from pyduel_engine.model.engine import Engine
    from pyduel_engine.epic_plugin.epic_states import Main

    app = QApplication(sys.argv)

    squads = [s_utils.setup_squad(1, Main.dooku, SqState.dark),
              s_utils.setup_squad(2, Main.mace, SqState.light)]
    board_type = BoardType.ruins
    engine = Engine({'squads': squads, 'board_type': board_type})
    engine.num_players = 2
    ex = BoardWidget(engine.board)
    ex.show()
    sys.exit(app.exec_())
