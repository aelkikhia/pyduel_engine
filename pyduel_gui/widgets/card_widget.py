import sys

from PyQt4.QtGui import QPushButton, QSizePolicy, QFont
from PyQt4.QtCore import QObject, SIGNAL


class CardWidget(QPushButton):
    def __init__(self, index, name=None, parent=None):
        super(CardWidget, self).__init__(parent)

        # Card data
        if name is None:
            self.setText('')
            # self.setText('{0}'.format(index))
        else:
            self.setText(name)
        self._index = index
        self._is_active = False
        self._is_selected = False

        # text and font
        self.font = QFont()
        self.font.setBold(True)
        self.font.setPixelSize(self.height() * 0.4)

        # dimensions
        policy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        self.setSizePolicy(policy)

        # set behavior
        self.setDisabled(True)
        self.setMouseTracking(True)
        self.setToolTip("game Card")
        QObject.connect(self, SIGNAL("clicked()"), self._onClick)

        # default color
        self._setColor()

    def set(self, index=None, name=None):
        self._index = index
        self.setText(name)

    def _onClick(self):
        self.emit(SIGNAL("select"), self._index)

    def select(self):
        """Select the square and change color """
        self._is_selected = True
        self._setColor()
        self.setDisabled(False)
        # print(self._is_activated)
        # print(self._is_selected)

    def deselect(self):
        """Deselect a square and change color """
        self._is_selected = False
        self._setColor()
        # print(self._is_activated)
        # print(self._is_selected)

    def activate(self):
        """Activate square set color, and make it selectable """
        self._is_active = True
        self._setColor()
        self.setDisabled(False)

    def deactivate(self):
        """Default color and disables """
        self._is_active = False
        self._setColor()
        self.setDisabled(True)

    def setEmpty(self):
        """Empties the square and deselects it until the next user input """
        self._is_selected = None
        self.setText('')
        # self.setText(str(self._index + 1))
        self._setColor()
        self.setDisabled(True)

    def _resetValues(self):
        """Deselects and deactivates """
        self._is_selected = False
        self._is_active = False

    def _setColor(self):
        """Sets the color of the square """
        # TODO: load color scheme outside the class
        if self._is_selected:
            state_color = 'Yellow'
        elif self._is_active:
            state_color = 'Purple'
        else:
            state_color = 'White'
        self.setStyleSheet("background-color:{0};".format(state_color))

    def resize_font(self):
        """Resize font with button size"""
        self.font.setPixelSize(self.height() * 0.4)
        self.setFont(self.font)

    def resizeEvent(self, event):
        """resize event for font... might need to account for other factors
        on resize"""
        self.resize_font()


if __name__ == '__main__':
    from PyQt4.QtGui import QApplication
    app = QApplication(sys.argv)
    widget = CardWidget(index=0, name='Monkey')
    widget.show()
    sys.exit(app.exec_())
