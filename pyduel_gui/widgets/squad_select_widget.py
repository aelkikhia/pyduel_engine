#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt4.QtGui import QSizePolicy, QHBoxLayout, QLabel, QCheckBox, \
    QComboBox, QWidget, QVBoxLayout
from pyduel_engine.content.engine_states import SqState
from pyduel_engine.utilities import squad_utilities
from pyduel_engine.epic_plugin.epic_states import Main

class SquadSelectWidget(QWidget):

    def __init__(self, num_players, parent=None):
        super(SquadSelectWidget, self).__init__(parent)
        self.num_players = num_players
        self._build_selections()

    def _build_selections(self):
        self.selectors = list()

        # set policy
        policy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        self.setSizePolicy(policy)

        # build layout
        self.vLayout = QVBoxLayout()
        for x in range(1, self.num_players + 1):

            horizontalLayout = QHBoxLayout()
            horizontalLayout.addWidget(QLabel("Player {0}".format(x)))
            sideCheckbox = QCheckBox(self)
            squadComboBox = QComboBox(self)

            for name, member in Main.__members__.items():
                squadComboBox.addItem(name.title(), member)

            horizontalLayout.addWidget(sideCheckbox)
            horizontalLayout.addWidget(squadComboBox)
            self.vLayout.addLayout(horizontalLayout)
            self.selectors.append({'side': sideCheckbox,
                                   'squad': squadComboBox})
        self.setLayout(self.vLayout)

    def getSquads(self):
        """return list of squads"""
        # TODO: fix the rest of the characters and make a choose character
        squads = []
        for index, elem in enumerate(self.selectors):
            if elem['squad'].currentIndex():

                if elem['side'].isChecked():
                    state = SqState.light
                else:
                    state = SqState.dark

                char = Main(elem['squad'].currentIndex())
                squads.append(squad_utilities.setup_squad(index, char, state))
        return squads

if __name__ == "__main__":
    import sys
    from PyQt4.QtGui import QApplication
    app = QApplication(sys.argv)
    ex = SquadSelectWidget(6)
    ex.show()
    sys.exit(app.exec_())
