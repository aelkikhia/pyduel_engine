#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt4 import QtGui


class CharWidget(QtGui.QWidget):

    def __init__(self, char, parent=None):
        """Char widget takes dictionary for label information
        'main': {'name': 'Dookie', 'hp': 17, 'max_hp': 20}"""

        super(CharWidget, self).__init__(parent)
        self.charButton = QtGui.QPushButton(char['name'])
        self.hpLabel = QtGui.QLabel(str(char['hp']))
        self.maxHPLabel = QtGui.QLabel("/ " + str(char['max_hp']))

        # Button dimensions
        button_policy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred,
                                          QtGui.QSizePolicy.Preferred)
        self.charButton.setSizePolicy(button_policy)

        # dimensions
        policy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred,
                                   QtGui.QSizePolicy.Preferred)
        self.setSizePolicy(policy)

        # TODO: add modifier to change HP
        # TODO: add button action to make active character on click
        # TODO: and highlight on map

        # layout
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.addWidget(self.charButton)
        self.horizontalLayout.addWidget(self.hpLabel)
        self.horizontalLayout.addWidget(self.maxHPLabel)
        self.setLayout(self.horizontalLayout)

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    char_stats = {'name': 'Dooku', 'hp': 17, 'max_hp': 20}
    ex = CharWidget(char_stats)
    ex.show()
    sys.exit(app.exec_())

