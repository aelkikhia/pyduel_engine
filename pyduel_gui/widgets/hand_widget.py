#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt4.QtGui import QWidget, QSizePolicy, QHBoxLayout, QVBoxLayout
from PyQt4.QtCore import SIGNAL, QObject
from pyduel_gui.widgets.card_widget import CardWidget

MAX_NUM_CARDS = 10


class HandWidget(QWidget):

    def __init__(self, parent=None):
        super(HandWidget, self).__init__(parent)
        self.cardButtons = []
        self.__buildControls()

    def setCards(self, hand_info):
        """shows the hand either with text or with images"""
        # TODO: add image support
        for index, card in enumerate(hand_info):
            self.cardButtons[index].set(index=index, name=card['name'])
        hand_size = len(hand_info)
        if hand_size < MAX_NUM_CARDS:
            for index in range(hand_size, MAX_NUM_CARDS):
                self.cardButtons[index].setEmpty()

    def getCard(self, card_index):
        """gets card button at index"""
        return self.cardButtons[card_index]

    def lockCards(self, selected_card=None):
        """Locks all cards and if active deactivates except for the selected
        card"""
        for card in self.cardButtons:
            card.deactivate()
        if selected_card is not None:
            self.cardButtons[selected_card].activate()

    def unlockCards(self, cards=None):
        """Lock down all other cards except the indexed card"""
        if cards is not None:
            for index in cards:
                self.cardButtons[index].activate()
        else:
            for card in self.cardButtons:
                card.activate()

    def cardSelect(self, index):
        """Select card at index """
        self.getCard(index).select()
        self.lockCards(index)

    def cardDeselect(self, index):
        """Deselect card at index """
        self.getCard(index).deselect()

    def onClick(self, index):
        # self.lockCards(index)
        self.emit(SIGNAL("cardClick"), index)

    def __buildControls(self):
        # layout
        verticalLayout = QVBoxLayout()
        horizontalLayout = QHBoxLayout()
        horizontalLayout2 = QHBoxLayout()
        # dimensions for buttons
        policy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)

        self.setSizePolicy(policy)

        for index in range(1, MAX_NUM_CARDS + 1):
            # create button
            card = CardWidget(index - 1)
            # create signal
            QObject.connect(card, SIGNAL("select"), self.onClick)
            # add button to list so we can access later
            self.cardButtons.append(card)
            if index < (MAX_NUM_CARDS + 1) / 2:
                horizontalLayout.addWidget(card)
            else:
                horizontalLayout2.addWidget(card)

        verticalLayout.addLayout(horizontalLayout)
        verticalLayout.addLayout(horizontalLayout2)
        self.setLayout(verticalLayout)
        # self.setLayout(horizontalLayout)

if __name__ == "__main__":
    import sys
    from PyQt4.QtGui import QApplication
    app = QApplication(sys.argv)
    hand_data = [{'name': '2|3'}, {'name': '5|1'},
                 {'name': '1|4'}, {'name': '1|4'}]
    hand = HandWidget()
    hand.setCards(hand_data)
    hand.show()
    sys.exit(app.exec_())
