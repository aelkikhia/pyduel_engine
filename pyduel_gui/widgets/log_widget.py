#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt4.QtGui import QTextEdit, QSizePolicy, QTextCursor

class LogWidget(QTextEdit):

    def __init__(self, parent=None):
        super(LogWidget, self).__init__(parent)
        policy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        self.setSizePolicy(policy)
        self.setToolTip("Game Log")
        self.setReadOnly(True)
        self.setLineWrapMode(self.NoWrap)
        self.moveCursor(QTextCursor.End)

if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication
    app = QApplication(sys.argv)
    win = LogWidget()
    win.show()
    sys.exit(app.exec_())
