import sys

from PyQt4.QtGui import QPushButton, QSizePolicy, QFont
from PyQt4.QtCore import QObject, SIGNAL

from pyduel_engine.content.engine_states import SqState as SqState


class Square(QPushButton):
    def __init__(self, position=None, state=SqState.empty, parent=None):
        super(Square, self).__init__(parent)

        # square data
        self._position = position
        self._state = state
        # temp values
        self._is_selected = False
        self._is_active = False

        # text and font
        self.font = QFont()
        self.font.setBold(True)
        self.font.setPixelSize(self.height() * 0.4)

        # dimensions
        policy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        policy.setHeightForWidth(True)
        self.setSizePolicy(policy)

        # set behavior
        self.setDisabled(True)
        self.setMouseTracking(True)
        self.setToolTip("game square")
        self.setStatusTip(self._position.__repr__())
        QObject.connect(self, SIGNAL("clicked()"), self._onClick)

        # default color
        self._setColor()

    def select(self):
        """Select the square and change color """
        self._is_selected = True
        self._setColor()
        self.setDisabled(False)
        # print(self._is_activated)
        # print(self._is_selected)

    def deselect(self):
        """Deselect a square and change color """
        self._is_selected = False
        self._setColor()
        # print(self._is_activated)
        # print(self._is_selected)

    def activate(self):
        """Activate square set color, and make it selectable """
        self._is_active = True
        self._setColor()
        self.setDisabled(False)

    def deactivate(self):
        """Default color and disables """
        self._is_active = False
        self._setColor()
        self.setDisabled(True)

    def setEmpty(self):
        """Empties the square and deselects it until the next user input """
        self._is_selected = None
        self._setState(SqState.empty)
        self.setText('')
        self._setColor()
        self.setDisabled(True)

    def setSquare(self, state=None, text=None):
        """Sets the square state and text. Resets all variables """
        if state is not None:
            self._state = state
            self._setColor()
        if text is not None:
            self.setText(text)
        self._is_active = False
        self.setDisabled(True)

    def _resetValues(self):
        """Deselects and deactivates """
        self._is_selected = False
        self._is_active = False

    def _setState(self, state):
        """Sets state color for board """
        self._state = state

    def _setColor(self):
        """Sets the color of the square """
        if self._is_active:
            state_color = 'Purple'
        elif self._is_selected:
            state_color = 'Yellow'
        else:
            state_color = {
                SqState.light: "Blue",
                SqState.dark: "Red",
                SqState.empty: "White",
                SqState.obstacle: "Grey",
                SqState.hole: "Black",
            }[self._state]
        self.setStyleSheet("background-color:{0};".format(state_color))

    ##########################################################################
    # Properties
    ##########################################################################

    def _onClick(self):
        """On click event"""
        self.emit(SIGNAL("select"), self._position)

    def _resizeFont(self):
        """Resize font with button size"""
        self.font.setPixelSize(self.height() * 0.4)
        self.setFont(self.font)

    def resizeEvent(self, event):
        """resize event for font... might need to account for other factors
        on resize"""
        self._resizeFont()

    def hasHeightForWidth(self):
        return True

    def heightForWidth(self, width):
        return width

if __name__ == '__main__':
    from PyQt4.QtGui import QApplication
    app = QApplication(sys.argv)
    widget = Square()
    widget.show()
    sys.exit(app.exec_())
