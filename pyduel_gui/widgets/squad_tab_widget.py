#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt4.QtGui import QTabWidget, QSizePolicy
from pyduel_gui.widgets.squad_widget import SquadWidget


class SquadTabWidget(QTabWidget):
    """This is a custom tab widget for displaying squads in sequential order
    of play, hopefully highlighting the tab color/text to specify at a glance
    which main characters are still alive"""

    def __init__(self, squads, parent=None):
        super(SquadTabWidget, self).__init__(parent)
        self._squads = []
        self._buildTab()
        self._populateTabs(squads)

    def _buildTab(self):
        policy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        self.setSizePolicy(policy)

    def _populateTabs(self, squads):
        """Populate the tabs with squad/character information."""
        # TODO:NOT sure about saving the number of squads as a variable
        # TODO: instead of just getting len() from the squads array
        for squad in squads:
            squad_widget = SquadWidget(squad)
            self._squads.append(squad_widget)
            self.addTab(squad_widget, squad['chars'][0]['name'])

    def updateTab(self, squad, index=None):
        if index is None:
            index = self.currentIndex()
        self._squads[index].updateTab(squad)




if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication
    squad_info = [{'player': 'Barnabie Jones', 'side': 'Dark', 'actions': 0,
                   'hand': 0, 'chars':
                       {'main': {'name': 'Dookie', 'hp': 17, 'max_hp': 20},
                        'minor1': {'name': 'Okie', 'hp': 2, 'max_hp': 5},
                        'minor2': {'name': 'dokie', 'hp': 1, 'max_hp': 5}}},
                  {'player': 'Barnabie Jones', 'side': 'Dark', 'actions': 0,
                   'hand': 0, 'chars':
                       {'main': {'name': 'Mac', 'hp': 17, 'max_hp': 20},
                        'minor1': {'name': 'Mic', 'hp': 2, 'max_hp': 5},
                        'minor2': {'name': 'Mock', 'hp': 1, 'max_hp': 5}}}]

    app = QApplication(sys.argv)
    ex = SquadTabWidget(squad_info)
    ex.show()
    sys.exit(app.exec_())
