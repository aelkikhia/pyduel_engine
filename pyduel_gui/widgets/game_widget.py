#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging

from PyQt4.QtGui import QWidget, QVBoxLayout, QHBoxLayout
from PyQt4.QtCore import QObject, SIGNAL

# Custom Widgets
from pyduel_gui.widgets.board_widget import BoardWidget
from pyduel_gui.widgets.dice_widget import DiceWidget
from pyduel_gui.widgets.action_widget import ActionWidget
from pyduel_gui.widgets.log_widget import LogWidget
from pyduel_gui.widgets.squad_tab_widget import SquadTabWidget
from pyduel_gui.widgets.hand_widget import HandWidget

# temporary settings from engine state
from pyduel_engine.content.engine_states import Action

MAX_HAND_SIZE = 10


class GameWidget(QWidget):
    """Instantiates game object, and all widgets, then calls game setup """

    def __init__(self, parent=None):
        super(GameWidget, self).__init__(parent)
        self.game = self.parent().game
        self.__build_controls()
        self.__build_layout()
        self._setupGame()

    #########################################################################
    # Game Phase Methods
    #########################################################################
    def _setupGame(self):
        """Setup game and update gui to match """
        self.game.setup()
        self._logGameEvent()
        self._charsPlace()
        self.startGame()

    def startGame(self):
        """Starts the game. """
        self._logGameEvent('Game Start')
        self._squadsUpdate()
        self._movePhaseStart()

    def _movePhaseStart(self):
        """Toggles move phase in game, shows hand """
        self._squadHandGuiUpdate()
        self.game.move_phase_start()
        self._logGameEvent()
        self._movePhaseSet()

    def _movePhaseEnd(self):
        """Called when the the active player has no more moves left
        Lock down the active squad, toggle move phase to false and lock the
        skip move button then calls the start of the action phase """
        self.game.move_phase_end()
        self._logGameEvent()
        self._tempComponentsReset()
        self._actionPhaseStart()

    def _actionPhaseStart(self):
        """starts the action phase """
        self.game.action_phase_start()
        self._logGameEvent()
        if self.game.active_squad().has_action():
            if self.game.active_squad().has_hand():
                self._actionPhaseSet()
        else:
            self._actionPhaseEnd()

    def _actionPhaseEnd(self):
        """Ends the action phase """
        self.game.action_phase_end()
        self._logGameEvent()
        self._lockActiveSquad()
        self._turnEnd()

    def _turnEnd(self):
        """Ends the turn incrementing what turn it is unless its the last turn
        in which we reset back to the first player and call for the new round
        """
        if self.game.turn == self.game.num_squads() - 1:
            self._roundEnd()
        else:
            self.game.increment_turn()
            self._logGameEvent()
            self._movePhaseStart()

    def _roundEnd(self):
        """End of round """
        self.game.increment_round()
        self._logGameEvent()
        self._movePhaseStart()

    ###########################################################################
    # Game and Gui modifiers
    ###########################################################################
    def _movePhaseSquareSelect(self, pos):
        """Move Phase Square selection.
        - if active char is not set. Set new active character at pos or,
          if active char pos is the input pos, move the character.
        - clean up the board if deselecting or moving """
        # TODO: Either lock down active squad or make condition for
        # switching active characters

        # select active character and targets
        if self.game.active_char is None:
            self._activeCharSelect(pos)
        else:
            # gui cleanup
            self._possibleTargetsDeactivate()
            self._squareDeselect(pos)
            # move
            if self.game.active_char.pos != pos:
                self._charMove(self.game.active_char, pos)
            # deselect active char
            self._tempComponentsReset()

    def _actionPhaseSquareSelect(self, pos):
        """Action Phase Square selection. difference from the move phase is
        after the second square is selected, the function ends """
        # select active character and targets
        if self.game.active_char is None:
            self._activeCharSelect(pos)
            self.hand.unlockCards(self.game.playable_cards)
            print(self.game.playable_cards)
        elif self.game.active_char.pos == pos:
            # gui cleanup
            if self.game.active_target is not None:
                self._squareDeselect(self.game.active_target.pos)
                self.game.active_target = None
            self._activeCharDeselect()
        else:
            self._actionPhaseTargetSelect(pos)

    def _actionPhaseTargetSelect(self, pos):
        """continuation method of action phase square select. Select or
        deselect target """
        if self.game.active_target is None:
            self._activeTargetSelect(pos)
        # Deselect active target
        elif self.game.active_target.pos == pos:
            self._activeTargetDeselect()

    def _actionPhaseCardSelect(self, card_index):
        if self.game.active_card() is None:
            self.game.set_active_card(card_index)
            self.hand.cardSelect(card_index)
        else:
            self.hand.cardDeselect(self.game.active_card())
            self.game.set_active_card(None)
            self.hand.unlockCards(self.game.playable_cards)

    ###########################################################################
    # Actions
    ###########################################################################
    def _actionDraw(self):
        """Clean up board if necessary, draw card, update the gui to reflect
        changes """
        if len(self.game.active_squad().hand) == MAX_HAND_SIZE:
            # TODO: discard card to draw
            self._discardCard()
        self.game.action_draw()
        self._logGameEvent()

    def _discardToHeal(self):
        pass

    def _actionPlayCard(self):
        # TODO: shift game logic out of gui
        if self.game.can_play_card():
            self.game.action_play_card()
            self._logGameEvent()

    def _tempComponentsReset(self):
        """Clear selected and active components """
        if self.game.active_char is not None:
            self._squareDeselect(self.game.active_char.pos)
            self.game.active_char = None
        if self.game.possible_targets is not None:
            self._possibleTargetsDeactivate()
            self.game.possible_targets = None
        if self.game.active_target is not None:
            self._squareDeselect(self.game.active_target.pos)
            self.game.active_target = None
        if self.game.active_char is None and \
                self.game.active_card() is not None:
            self.hand.cardDeselect(self.game.active_card())
            self.hand.lockCards()
            self.game.set_active_card(None)

    def _charMove(self, char, pos):
        """moves character to new position in game and gui. Gui needs to change
        before the game, since the char.pos will be overwritten if the game
        changes first """
        self.board.moveChar(char.pos, pos, char.side, char.name.split()[-1])
        self.game.move_char(char, pos)
        self._logGameEvent()
        self._squadUpdate()

        if self.game.num_moves < 1:
            self._movePhaseEnd()

    def _activeCharSelect(self, pos):
        """Select active character in game and gui and activate possible
        targets Called in the move phase and action phase for active char
        select """
        self.game.active_char_select(pos)
        self._squareSelect(pos)
        self._possibleTargetsActivate()

    def _activeCharDeselect(self):
        """Deselect active char and deactivate possible targets """
        if not self.game.is_move_phase():
            self.hand.lockCards()
        self._possibleTargetsDeactivate()
        self._squareDeselect(self.game.active_char.pos)
        self._tempComponentsReset()

    def _activeTargetSelect(self, pos):
        """deactivate possible targets, select target in game and gui. Called
        by the _actionPhaseTargetSelection() """
        self._possibleTargetsDeactivate()
        self.game.set_active_target(pos)
        self._squareSelect(pos)

    def _activeTargetDeselect(self):
        """Deselect active target and activate possible targets """
        self._squareDeselect(self.game.active_target.pos)
        self._possibleTargetsActivate()
        self.game.active_target = None

    def _movePhaseSet(self):
        """locks action buttons, unlocks active squad and skip move button """
        self._diceUpdate()
        self.actions.disableActions()
        self.actions.skip.setDisabled(False)
        self._unlockActiveSquad()

    ###########################################################################
    # Widget/View Modifiers
    ###########################################################################
    def _actionPhaseSet(self):
        """Update action phase widgets """
        self._actionButtonsUnlock()
        self.actions.skip.setDisabled(True)
        self._unlockActiveSquad()

    ######################### Action Widget ###################################
    def _actionButtonsUnlock(self):
        """Locks and unlocks buttons based on actions available """
        # lock or unlock action buttons if able
        if self.game.active_squad().can_draw_card():
            # unlocks draw ability
            self.actions.draw.setDisabled(False)

        # TODO: might be unnecessary since the hand is already visible
        if self.game.active_squad().has_hand():
            # unlock play button
            self.actions.play.setDisabled(False)

        if self.game.active_squad().can_heal_main() or \
                self.game.active_squad().can_heal_minor():
            # unlock heal button
            self.actions.heal.setDisabled(False)

    ###########################################################################
    # widget updaters
    ###########################################################################
    def _squadUpdate(self, squad_index=None):
        """Refresh squad information after all actions """
        if squad_index is None:
            self.squads.updateTab(self.game.active_squad().to_json())
            self.squads.setCurrentIndex(self.game.turn)
        else:
            self.squads.updateTab(self.game.squads()[squad_index].to_json())
            self.squads.setCurrentIndex(squad_index)

    def _squadsUpdate(self):
        """Refreshes all squad information"""
        for index, squad in enumerate(self.game.squads()):
            self._squadUpdate(index)
        # selects current squad tab
        self.squads.setCurrentIndex(self.game.turn)

    def _diceUpdate(self):
        """Sets the dice widget updates and deactivates """
        self.dice.set_result(self.game.dice().num(), self.game.dice().is_all())

    def _handUpdate(self, squad_index=None):
        """get list of all locked and unlocked cards """
        if squad_index is None:
            hand_json = [card.to_json()
                         for card in self.game.active_squad().hand]
        else:
            hand_json = [card.to_json()
                         for card in self.game.squads()[squad_index].hand]
        # print(hand_json)
        self.hand.setCards(hand_json)

    def _squadHandGuiUpdate(self):
        """Update hand, squad gui """
        self._squadUpdate()
        self._handUpdate()

    def _logGameEvent(self, info=None):
        """Appends state changes and events to the log """
        if info is None:
            self.log.append(self.game.game_event)
        else:
            self.log.append(info)

    ###########################################################################
    #  Game Setup methods
    ###########################################################################
    def _charsPlace(self):
        """Place all characters on the board gui """
        for squad in self.game.squads():
            self._charPlace(squad.chars[0], squad.chars[0].pos)
            list_pos = self.game.get_adj_empty_pos(squad.chars[0].pos)
            self._charPlace(squad.chars[1], list_pos[0])
            self._charPlace(squad.chars[2], list_pos[1])

    def _charPlace(self, char, pos):
        """places character in new position in game and gui """
        self.game.char_place(char, pos)
        self._logGameEvent()
        self.board.placeChar(pos, char.side, char.name.split()[-1])

    #########################################################################
    # Event/Selection Methods
    #########################################################################
    def _actionClicked(self, action_type):
        """Action button click responses """
        # TODO: move the skip button out of actions
        if action_type == Action.skip:
            self._movePhaseEnd()
        else:
            if action_type == Action.draw:
                self._actionDraw()
            elif action_type == Action.heal:
                self._discardToHeal()
            else:
                self._actionPlayCard()

            self._tempComponentsReset()
            self._squadHandGuiUpdate()
            if not self.game.active_squad().has_action():
                self._actionPhaseEnd()

    def _squareClicked(self, pos):
        """Select active square or target square """
        if self.game.is_move_phase():
            self._movePhaseSquareSelect(pos)
        else:
            self._actionPhaseSquareSelect(pos)

    def _cardClicked(self, card_index):
        """Card click event"""
        self._actionPhaseCardSelect(card_index)

    ############################# Board Widget ################################
    def _lockActiveSquad(self):
        """locks the active squad characters on the board """
        self.board.lockSquares(self.game.get_active_chars())

    def _unlockActiveSquad(self):
        """unlocks the active squad characters on the board """
        self.board.unlockSquares(self.game.get_active_chars())

    def _squareSelect(self, pos):
        """Select the active character in game and gui """
        self.board.squareSelect(pos)

    def _squareDeselect(self, pos):
        """Deselect the active character in game and gui """
        self.board.squareDeselect(pos)

    def _possibleTargetsDeactivate(self):
        """Deactivate possible targets """
        self.board.deactivateSquares(self.game.possible_targets)

    def _possibleTargetsActivate(self):
        """Activate possible targets """
        self.board.activateSquares(self.game.possible_targets)

    ###########################################################################
    #  Game Widget methods
    ###########################################################################
    def __build_controls(self):
        """Builds the widgets and the connections to the widgets """
        self.board = BoardWidget(self.game.board())
        QObject.connect(self.board, SIGNAL("select"), self._squareClicked)
        self.hand = HandWidget()
        QObject.connect(self.hand, SIGNAL("cardClick"), self._cardClicked)
        self.squads = SquadTabWidget(self.game.squads_json())
        # QObject.connect(self.squad, SIGNAL("squadSignal"), self._squadSignal)

        self.actions = ActionWidget(self)
        QObject.connect(self.actions, SIGNAL("actionClicked"),
                        self._actionClicked)

        self.dice = DiceWidget()
        self.log = LogWidget()

    def __build_layout(self):
        """builds the layouts of the widgets """
        self.verticalLayout = QVBoxLayout()
        self.vertical2Layout = QVBoxLayout()
        self.horizontalLayout = QHBoxLayout()
        self.horizontal2Layout = QHBoxLayout()

        self.actionDiceHLayout = QHBoxLayout()
        self.actionDiceHLayout.setSpacing(0)
        self.actionDiceHLayout.addWidget(self.actions)
        self.actionDiceHLayout.addWidget(self.dice)

        self.boardVLayout = QVBoxLayout()
        self.boardVLayout.setSpacing(0)
        self.boardVLayout.addWidget(self.board)
        # self.boardCardVLayout.addWidget(self.hand)

        self.horizontalLayout.addLayout(self.boardVLayout)
        self.vertical2Layout.addWidget(self.log)
        self.vertical2Layout.addWidget(self.squads)
        self.vertical2Layout.addWidget(self.hand)
        self.vertical2Layout.addLayout(self.actionDiceHLayout)

        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout.addLayout(self.vertical2Layout)
        self.verticalLayout.addLayout(self.horizontal2Layout)
        self.setLayout(self.verticalLayout)


if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication

    app = QApplication(sys.argv)
    from pyduel_engine.content.engine_states import BoardType
    from pyduel_engine.utilities import squad_utilities as s_utils
    from pyduel_engine.content.engine_states import SqState as SqState
    from pyduel_engine.model.engine import Engine
    from pyduel_engine.epic_plugin.epic_states import Main
    squads = [s_utils.setup_squad(1, Main.dooku, SqState.dark),
              s_utils.setup_squad(2, Main.mace, SqState.light)]
    board_type = BoardType.ruins
    test_engine = Engine({'squads': squads, 'board_type': board_type})
    test_engine.num_players = 2

    win = GameWidget(test_engine)
    win.show()
    sys.exit(app.exec_())
