#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt4.QtGui import QLabel

class StatusBar(QLabel):
    def __init__(self, status, parent=None):
        super(StatusBar, self).__init__(parent)
        self.setToolTip("game status bar")
        self.setText(status)

    def setStatus(self, status):
        self.setText(status)

if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication
    app = QApplication(sys.argv)
    widget = StatusBar("R: {} T: {}\tPhase: {}".format(1, 2, 'Move'))
    widget.show()

    sys.exit(app.exec_())
