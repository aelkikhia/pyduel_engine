#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt4 import QtGui


class DiceWidget(QtGui.QPushButton):

    def __init__(self, parent=None):
        super(DiceWidget, self).__init__(parent)
        self._buildDie()

    def _buildDie(self):
        # dimensions
        self.resize(70, 70)

        policy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred,
                                   QtGui.QSizePolicy.Preferred)
        policy.setHeightForWidth(True)
        self.setSizePolicy(policy)

        # text and font
        self.font = QtGui.QFont()
        self.font.setBold(True)
        self.font.setPixelSize(self.height() * 0.4)

        # default color/text
        self.setText("Dice")
        self.setStyleSheet("background-color: rgb(135, 5, 45);")

    def resize_font(self):
        """Resize font with button size """
        self.font.setPixelSize(self.height() * 0.4)
        self.setFont(self.font)

    def resizeEvent(self, event):
        """resize event """
        self.resize_font()

    def set_result(self, value, is_all):
        """prints the dice value from the engine"""
        if is_all:
            self.setStyleSheet("background-color: rgb(135, 5, 45);")
            self.setText("{0}\nAll".format(value))
        else:
            self.setStyleSheet("background-color: rgb(5, 135, 10);")
            self.setText("{0}".format(value))
        self.setDisabled(True)


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    die = DiceWidget()
    die.set_result(4, True)
    die.show()

    sys.exit(app.exec_())

