#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt4.QtGui import QIcon, QApplication, QAction, QMenuBar
from PyQt4.QtCore import QObject, SIGNAL


class GameMenu(QMenuBar):

    def __init__(self, parent=None):
        super(GameMenu, self).__init__(parent)
        self.__set_actions()
        self.__menu()

    def __set_actions(self):
        """Creates actions and keyboard shortcuts """
        # Exit
        self.exitAction = QAction(QIcon('exit.png'), '&Exit', self)
        self.exitAction.setShortcut('Ctrl+Q')
        self.exitAction.setStatusTip('Exit application')
        QObject.connect(self.exitAction, SIGNAL("triggered()"),
                        QApplication.quit)

        # save
        self.startAction = QAction('&Start', self)
        self.startAction.setShortcut('Ctrl+E')
        self.startAction.setStatusTip('Start Game')
        QObject.connect(self.startAction, SIGNAL("triggered()"),
                        self.parent().start_game)

        # open
        self.openAction = QAction('&Open', self)
        self.openAction.setShortcut('Ctrl+O')
        self.openAction.setStatusTip('Open File')
        QObject.connect(self.openAction, SIGNAL("triggered()"),
                        self.parent().open_file)

        # New Game
        self.newGameAction = QAction('&New Game', self)
        self.newGameAction.setShortcut('Ctrl+N')
        self.newGameAction.setStatusTip('New Game')
        QObject.connect(self.newGameAction, SIGNAL("triggered()"),
                        self.parent().setup_dialog)

    def __menu(self):
        """creates the menu for the game"""

        # File menu
        self.fileMenu = self.addMenu('&File')
        self.fileMenu.addAction(self.newGameAction)
        self.fileMenu.addAction(self.startAction)
        self.fileMenu.addAction(self.openAction)
        self.fileMenu.addAction(self.exitAction)

        # Settings menu
        self.settingsMenu = self.addMenu('&Settings')

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    win = GameMenu()
    win.show()
    sys.exit(app.exec_())
