#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt4.QtGui import QWidget, QSizePolicy, QHBoxLayout, QPushButton
from PyQt4.QtCore import SIGNAL, QObject
from pyduel_engine.content.engine_states import Action


class ActionWidget(QWidget):

    def __init__(self, parent=None):
        super(ActionWidget, self).__init__(parent)
        self._build_controls()
        self._build_layout()
        self.disableActions()

    def _build_controls(self):
        """Action controls """
        self.draw = QPushButton("Draw\nCard")
        QObject.connect(self.draw, SIGNAL("clicked()"), self.drawClicked)

        self.play = QPushButton("Play\nCard")
        QObject.connect(self.play, SIGNAL("clicked()"), self.playClicked)

        self.heal = QPushButton("Heal\n")
        QObject.connect(self.heal, SIGNAL("clicked()"), self.healClicked)

        self.skip = QPushButton("Skip\nMove")
        QObject.connect(self.skip, SIGNAL("clicked()"), self.skipClicked)

    def _build_layout(self):
        policy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        self.setSizePolicy(policy)
        self.horizontalLayout = QHBoxLayout()

        # Button dimensions
        self.draw.setSizePolicy(policy)
        self.play.setSizePolicy(policy)
        self.heal.setSizePolicy(policy)
        self.skip.setSizePolicy(policy)

        # layout
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.addWidget(self.draw)
        self.horizontalLayout.addWidget(self.play)
        self.horizontalLayout.addWidget(self.heal)
        self.horizontalLayout.addWidget(self.skip)
        self.setLayout(self.horizontalLayout)

    def drawClicked(self):
        self.emit(SIGNAL("actionClicked"), Action.draw)

    def playClicked(self):
        self.emit(SIGNAL("actionClicked"), Action.play)

    def healClicked(self):
        self.emit(SIGNAL("actionClicked"), Action.heal)

    def skipClicked(self):
        self.emit(SIGNAL("actionClicked"), Action.skip)

    def disableActions(self):
        self.draw.setDisabled(True)
        self.play.setDisabled(True)
        self.heal.setDisabled(True)
        self.skip.setDisabled(True)

if __name__ == "__main__":
    import sys
    from PyQt4.QtGui import QApplication
    app = QApplication(sys.argv)
    ex = ActionWidget()
    ex.show()
    sys.exit(app.exec_())
