#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt4.QtGui import QWidget, QVBoxLayout, QSizePolicy, QLabel, QPushButton
from pyduel_gui.widgets.char_widget import CharWidget


class SquadWidget(QWidget):

    def __init__(self, squad, parent=None):
        """Squad widget that takes dictionary input to populate stats
         dict: squad = {'player': 0, 'side': 0, 'actions': 0, 'hand': 0}"""
        super(SquadWidget, self).__init__(parent)
        self.squad = squad

        # dimensions
        policy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        self.setSizePolicy(policy)

        # layout
        self.VerticalLayout = QVBoxLayout()

        # labels for squad stats
        self.playerLabel = QLabel("Player: {0}".format(squad['player']))
        self.sideLabel = QLabel("Side: {0}".format(squad['side']))
        self.actionLabel = QLabel("Actions: {0}".format(squad['actions']))
        self.handButton = QPushButton("Hand: {0}".format(len(squad['hand'])))

        # add to layout
        self.VerticalLayout.addWidget(self.playerLabel)
        self.VerticalLayout.addWidget(self.sideLabel)
        self.VerticalLayout.addWidget(self.actionLabel)
        self.VerticalLayout.addWidget(self.handButton)
        self.VerticalLayout.addWidget(CharWidget(squad['chars'][0]))
        self.VerticalLayout.addWidget(CharWidget(squad['chars'][1]))

        if squad['chars'][2]:
            self.VerticalLayout.addWidget(CharWidget(squad['chars'][2]))

        # set layout
        self.setLayout(self.VerticalLayout)

    def updateTab(self, squad):
        self.handButton.setText("Hand: {0}".format(len(squad['hand'])))
        self.actionLabel.setText("Actions: {0}".format(squad['actions']))

if __name__ == "__main__":

    import sys
    from PyQt4.QtGui import QApplication
    app = QApplication(sys.argv)
    squad_info = {
        'player': 'Barnabie Jones', 'side': 'Dark', 'actions': 0, 'hand': 0,
        'chars': {'main': {'name': 'Dookie', 'hp': 17, 'max_hp': 20},
                  'minor1': {'name': 'Okie', 'hp': 2, 'max_hp': 5},
                  'minor2': {'name': 'dokie', 'hp': 1, 'max_hp': 5}}}
    ex = SquadWidget(squad_info)
    ex.show()
    sys.exit(app.exec_())
