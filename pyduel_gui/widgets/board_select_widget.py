#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt4 import QtGui
# from pyduel_engine.content.engine_states import BoardType

class BoardSelectWidget(QtGui.QComboBox):

    def __init__(self, board_types, parent=None):
        super(BoardSelectWidget, self).__init__(parent)
        self.board_types = board_types
        # dimensions
        policy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred,
                                   QtGui.QSizePolicy.Preferred)
        self.setSizePolicy(policy)

        # populate
        for name, member in self.board_types.__members__.items():
            self.addItem(name.title(), member)

    def getBoard(self):
        """Return BoardType selection"""
        return self.currentIndex()

if __name__ == "__main__":

    import sys
    from pyduel_engine.content.engine_states import BoardType
    app = QtGui.QApplication(sys.argv)
    ex = BoardSelectWidget(BoardType)
    ex.show()
    sys.exit(app.exec_())
