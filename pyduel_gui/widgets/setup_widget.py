#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt4.QtGui import QDialog, QVBoxLayout, QDialogButtonBox
from PyQt4.QtCore import QObject, Qt, SIGNAL
from pyduel_gui.widgets import board_select_widget, squad_select_widget
from pyduel_engine.content.engine_states import BoardType

from pyduel_engine.model.engine import Engine

MAX_NUM_PLAYERS = 6

class SetupWidget(QDialog):
    def __init__(self, parent=None):
        super(SetupWidget, self).__init__(parent)
        self.__buildDialog()

    def __buildDialog(self):
        self.boardComboBox = board_select_widget.BoardSelectWidget(BoardType)
        self.squad_selector = squad_select_widget.SquadSelectWidget(
            MAX_NUM_PLAYERS)

        mainVlayout = QVBoxLayout(self)
        mainVlayout.addWidget(self.boardComboBox)
        mainVlayout.addWidget(self.squad_selector)

        # OK and Cancel buttons
        self.buttonBox = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel, Qt.Horizontal, self)
        mainVlayout.addWidget(self.buttonBox)

        QObject.connect(self.buttonBox, SIGNAL("accepted()"), self.accept)
        QObject.connect(self.buttonBox, SIGNAL("rejected()"), self.reject)

    def getEngine(self):
        squads = self.squad_selector.getSquads()
        board = BoardType(self.boardComboBox.getBoard())
        engine = Engine({'squads': squads, 'board_type': board})
        engine.num_players = len(engine.squads)
        return engine

    # static method to create the dialog and return (date, time, accepted)
    def engineSetup(self):
        dialog = SetupWidget(self.parent())
        exec_result = dialog.exec_()
        configured_engine = dialog.getEngine()
        return configured_engine, exec_result == QDialog.Accepted


if __name__ == "__main__":
    import sys
    from PyQt4.QtGui import QApplication
    app = QApplication(sys.argv)
    widget = SetupWidget()
    engine, result = widget.engineSetup()
    print("{} {}".format(engine, result))
    sys.exit(app.exec_())

